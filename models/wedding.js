var mongoose = require('mongoose');
var Schema = mongoose.Schema;

// create a schema
var weddingSchema = new Schema({
	registered: {
		type: Boolean,
		default: false
	},
    bigDay: { 
		type: Date,
		default: Date.now
	},
	users: [{
		type: Schema.ObjectId,
		ref: 'User'
	}],
	milestones: [{
		type: Schema.ObjectId,
		ref: 'Milestone'
	}],
	tasks: [{
		type: Schema.ObjectId,
		ref: 'Task'
	}],
    created_at: { 
		type: Date
	},
    updated_at: { 
		type: Date
	}
});

// on every save, add the date
weddingSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

	 // set fixed UTC hours
	 this.bigDay.setUTCHours(12,0,0);

    next();
});

// the schema is useless so far
// we need to create a model using it
var Wedding = mongoose.model('Wedding', weddingSchema);

// make this available to our users in our Node applications
module.exports = Wedding;
