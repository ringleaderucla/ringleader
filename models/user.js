var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var passportLocalMongoose = require('passport-local-mongoose-email');
var relationship = require("mongoose-relationship");

// create a schema
var userSchema = new Schema({
	//email and password field automatically generated
	wedding: {
		type: Schema.ObjectId,
		required: true,
		ref: 'Wedding',
		childPath: 'users'
	},
    firstName: { 
		type: String,
		required: true
	},
	lastName: { 
		type: String,
		required: true
	},
	role: { 
		type: String,
		required: true,
		enum: ['Administrator', 'Helper', 'Participant'] //Assumption: Highest privileges to lowest
	},
    admin: { 
		type: Boolean,
		required: true
	},
	permissions: { 
		//by default ALL users will see their own milestones and tasks that they are assigned to and that they've created
		//the read permissions refer to references outside of this user
		read: {
			milestone: { type: Boolean, default: false },
			task: { type: Boolean, default: false }
		},
		//ability to add new milestones and tasks
		write: {
			milestone: { type: Boolean, default: false },
			task: { type: Boolean, default: false }
		}
		//milestones/tasks can only be modified/deleted by admin and the user that created it
	},
	milestones: [{
		type: Schema.ObjectId,
		ref: 'Milestone'
	}],
	tasks: [{
		type: Schema.ObjectId,
		ref: 'Task'
	}],
	dailyDigest: {
		type: Boolean,
		default: true
	},
    created_at: { 
		type: Date
	},
    updated_at: { 
		type: Date
	}
});

// on every save
userSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

	//makes sure the user's role matches an enum value
	var enums = userSchema.path('role').enumValues;
	if (enums.indexOf(this.role) < 0) {
		if (this.admin)
			this.role = enums[0];
		else
			this.role = enums[enums.length - 1];
	}
	
	//set permissions depending on role
	switch (enums.indexOf(this.role)) {
		case 0:{
			//set all permission to true
			this.permissions.read.milestone = true;
			this.permissions.read.task = true;
			this.permissions.write.milestone = true;
			this.permissions.write.task = true;
			break;
		}
		case 1:{
			this.permissions.read.milestone = false;
			this.permissions.read.task = false;
			this.permissions.write.milestone = false;
			this.permissions.write.task = true;
			break;
		}
		case 2: {
			this.permissions.read.milestone = false;
			this.permissions.read.task = false;
			this.permissions.write.milestone = false;
			this.permissions.write.task = false;
			break;
		}
		default: {
			console.log("WARNING: Undefined switch case for permissions with role: " + this.role);
			//set all permission to false
			this.permissions.read.milestone = false;
			this.permissions.read.task = false;
			this.permissions.write.milestone = false;
			this.permissions.write.task = false;
			break;
		}
	}

    next();
});

userSchema.plugin(passportLocalMongoose, { 
	usernameField: 'email', 
	hashField: 'password', 
	populateFields: 'wedding' 
});

userSchema.plugin(relationship, { relationshipPathName: 'wedding' });

// the schema is useless so far
// we need to create a model using it
var User = mongoose.model('User', userSchema);

// make this available to our users in our Node applications
module.exports = User;
