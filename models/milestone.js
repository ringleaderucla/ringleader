var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require('mongoose-relationship');

// create a schema with four required fields: Wedding, Name, PersonInCharge,
// DueDate, and Description
var milestoneSchema = new Schema({
	wedding: {
		type: Schema.ObjectId,
		ref: 'Wedding',
		childPath: 'milestones',
		required: true,
	},
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	createdBy: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true
	},
	personInCharge: {
		type: Schema.ObjectId,
		required: true,
		ref: 'User',
		childPath: 'milestones'
	},
	dueDate: {
		type: Date,
		required: true
	},
	tasks: [{
		type: Schema.ObjectId,
		ref: 'Task'
	}],
	status: {
		type: Boolean,
		default: false
	},
	statusChangeDate: {
		type: Date,
		default: null
	},
	created_at: {
		type: Date
	},
	updated_at: {
		type: Date
	}
});

// on every save, add the date
milestoneSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;
	
	 // set fixed UTC hours
	this.dueDate.setUTCHours(12,0,0);
	var milestoneDueDate = this.dueDate;

    this.model('Task').find({_id: {$in: this.tasks}}, function(err, tasks) {
		if (err) console.log(err);
		for (var i = 0; i < tasks.length; i++) {
			//if the task belonging to this milestone has its dueDate greater than the milestone's dueDate then set the dueDate to this milestone's dueDate
			if (tasks[i].dueDate > milestoneDueDate) {
				tasks[i].dueDate = milestoneDueDate;
				tasks[i].updateDescription = "Due date changed";
				tasks[i].save(function(err) {
					if (err) console.log(err);
				});
			}
		}
		next();
	});
});

milestoneSchema.pre('remove', function(next) {
	//remove all associated tasks
    this.model('Task').remove({_id: {$in: this.tasks}}, function(err) {
		if (err) console.log(err);
		next();
	});
});

milestoneSchema.plugin(relationship, { relationshipPathName: ['wedding', 'personInCharge'] });

// we need to create a model using the schema
var milestoneModel = mongoose.model('Milestone', milestoneSchema);

// make this available to our users in our Node applications
module.exports = milestoneModel;
