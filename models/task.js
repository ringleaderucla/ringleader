var mongoose = require('mongoose');
var Schema = mongoose.Schema;
var relationship = require("mongoose-relationship");

// create a schema
var taskSchema = new Schema({
	wedding: {
		type: Schema.ObjectId,
		required: true,
		ref: 'Wedding',
		childPath: 'tasks'
	},
	name: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	createdBy: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true
	},
	personInCharge: {
		type: Schema.ObjectId,
		ref: 'User',
		required: true,
		childPath: 'tasks'
	},
	parentMilestone: {
		type: Schema.ObjectId,
		ref: 'Milestone',
		required: true,
		childPath: 'tasks'
	},
	dueDate: { 
		type: Date,
		required: true
	},
	status: {
		type: Boolean,
		default: false
	},
	statusChangeDate: {
		type: Date,
		default: null
	},
	updateDescription: {
		type: String,
		default: ""
	},
    created_at: { 
		type: Date
	},
    updated_at: { 
		type: Date
	}
});

// on every save, add the date
taskSchema.pre('save', function(next) {
    // get the current date
    var currentDate = new Date();

    // change the updated_at field to current date
    this.updated_at = currentDate;

    // if created_at doesn't exist, add to that field
    if (!this.created_at)
        this.created_at = currentDate;

	 // set fixed UTC hours
	this.dueDate.setUTCHours(12,0,0);

    next();
});

taskSchema.plugin(relationship, { relationshipPathName: ['wedding', 'parentMilestone', 'personInCharge'] });

// the schema is useless so far
// we need to create a model using it
var Task = mongoose.model('Task', taskSchema);

// make this available to our users in our Node applications
module.exports = Task;
