/*
Initializes tags with Bootstrap's popover toggle by calling the popover function and setting onclick to return false so that it does nothing

Vertically centers the container if the container is smaller than the empty window space given
else it will use fixed percentage for the top margin so that it doesn't overlap with the navigation bar
*/

//<th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="dateSorter">Some header name</th>
//<td data-time={Object.dueDate.getTime()}>Any date format</td>
function dateSorter(a, b) {
    if (a.time < b.time) return -1;
    if (a.time > b.time) return 1;
    return 0;
}

function recalculateMarginAndTransform(mainBody) {
	//fix for Safari, since window height on Safari gives incorrect value just set margin top to 5% as compromise
	if (navigator.userAgent.indexOf("Safari") != -1 && navigator.userAgent.indexOf("Chrome") < 0) {
		mainBody.style.marginTop = "5%";
		return;
	}
	var emptyHeight = $(window).height() - document.getElementById('navigation-bar').clientHeight;
	if (emptyHeight < mainBody.clientHeight) {
		mainBody.style.marginBottom = mainBody.style.marginTop = "2%";
		mainBody.style.transform = "none";
	}
	else {
		mainBody.style.marginTop = (emptyHeight / 2).toString() + "px";
		mainBody.style.transform = "translateY(-50%)";
	}
}

function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();

		//Init popover
		var popovers = $('[data-toggle="popover"]');
		for (var i = 0; i < popovers.length; i++) {
			popovers[i].onclick = function() { return false; }
		}
		popovers.popover();   

		//vertically center page
		var mainBody = document.getElementById("main-body");
		//recalculates mainBody's margin and transform if the window or mainBody's size changes
		window.onresize = function() { recalculateMarginAndTransform(mainBody); }
		mainBody.onresize = window.onresize;
		recalculateMarginAndTransform(mainBody);
	}
}
load();

