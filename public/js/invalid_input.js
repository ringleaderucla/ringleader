/*/USE:
<script src="/js/invalid_input.js" data-valid-fields='["fieldID1","fieldID2"]' data-invalid-field="fieldID3" data-err-msg="Field3 error" />

In chrome this script will show the invalidity popup message for the particular invalid-field
In other browsers this script will show a red text underneath the field with an invalid message
*/
function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		//get this script's attributes
		var validFields = thisScript.getAttribute('data-valid-fields'); //fields that come before invalidField
		var invalidField = document.getElementById(thisScript.getAttribute('data-invalid-field')); //field to actually highlight
		var errMsg = thisScript.getAttribute('data-err-msg'); //error message to be displayed
		var submitBtn = thisScript.getAttribute('data-submit');
		if (!submitBtn) //default submit button id
			submitBtn = "submit";
		if (navigator.userAgent.indexOf("Chrome") != -1) {
			//parse data string into array
			validFields = JSON.parse(validFields);
			//get submit button
			submitBtn = document.getElementById(submitBtn);

			//sets required field to true or false
			function setRequired(fields, req) {
				if (!fields)
					return;
				for (var i = 0; i < fields.length; i++) {
					document.getElementById(fields[i]).required = req;
				}
			}

			//disable requirements for each validField
			setRequired(validFields, false);

			//set invalid field to empty so that invalidity is true
			invalidField.value = "";
			//set and display invalid message
			invalidField.oninvalid = function() { this.setCustomValidity(errMsg); };
			submitBtn.click();
			//enable requirements for each validField
			setRequired(validFields, true);
		}
		else {
			document.getElementById(invalidField.id + '-err').innerHTML = errMsg;
		}
		//highlight invalid field
		invalidField.focus();
	}
}
load();
