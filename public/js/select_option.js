/*
Sets the selected index of an option within a select tag
*/
function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		var selectLayout = document.getElementById(thisScript.getAttribute("data-select-id"));
		selectLayout.selectedIndex = thisScript.getAttribute("data-option");
	}
}
load();
