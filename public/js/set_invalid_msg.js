/*
Sets the custom invalidity message so that it pops up when the user tries to submit a bad input
*/
function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();

		var elem = document.getElementById(thisScript.getAttribute('data-id'));

		//sets invalid message
		elem.oninvalid = function() { this.setCustomValidity(this.title); };
		elem.oninput = function() { this.setCustomValidity(''); };
		if (elem.value && elem.value != "") { //if the element already has a value then reset validity message
			elem.oninput();
		}
	}
}
load();
