function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	//**In JSX**
	//<script src="/js/template.js" data-something="someData" />
	//**Then in onload**
	//var someData = thisScript.getAttribute('data-something');
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		//TODO: code here
	}
}
load();
