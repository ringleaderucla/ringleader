import Calendar from 'react-input-calendar'

/*
Used in Add/Edit Milestone and Task when choosing a due date
It will prevent the user from selecting a date that is before the current date
and it will prevent the user from choosing a date after the wedding date
or if it is a task it cannot be after the parent milestone's due date
*/

var page = function() {
return(
	<div className="form-group has-feedback">
  		<label className="control-label" htmlFor="date">{this.props.label}</label>
		<Calendar ref={(calendar) => this.calendar = calendar} onChange={this.onCalendarChange} id="date" minDate={new Date()} maxDate={this.state.maxDate} 
			hideIcon={true} inputFieldClass="form-control" inputName="date" inputFieldId="date" format='MM/DD/YYYY' openOnInputFocus={true} date={this.state.date} />
		<div className="row invalid-red" id="date-err">{this.props.badDate ? "The date must be between today and " + this.state.maxDate.toLocaleDateString() : ""}</div>
		<i className="glyphicon glyphicon-calendar form-control-feedback"></i>
	</div>
)};

var CalendarComponent = React.createClass({
	getInitialState: function() {
		return {
			date: this.props.date ? this.props.date : new Date(),
			maxDate: new Date(this.props.maxDate)
		};
	},
	onCalendarChange: function(date) {
		if (new Date(date) > this.state.maxDate) { 
			this.setState({date: this.state.maxDate, maxDate: this.state.maxDate});
		}
	},
	onMilestoneChange: function() {
		var dates = document.getElementById('milestone').value.match(/[\d]+\/[\d]+\/[\d]+/g);
		var maxDate = new Date(dates[dates.length - 1]);
		if (new Date(this.calendar.state.date) > maxDate) {
			this.setState({date: maxDate, maxDate: maxDate});
		}
		else
			this.setState({date: this.calendar.state.date, maxDate: maxDate});
	},
	componentDidMount: function() {
		var milestoneSelect = document.getElementById('milestone');
		if (milestoneSelect) {
			milestoneSelect.onchange = this.onMilestoneChange;
			this.onMilestoneChange();
		}
	},
    render: page
});

var mountNode = document.getElementById('input-calendar');

ReactDOM.render(
	<CalendarComponent date={mountNode.getAttribute("data-date")} label={mountNode.getAttribute("data-label")} maxDate={mountNode.getAttribute("data-max-date")} badDate={mountNode.getAttribute("data-bad-date")} />,
	mountNode
);
 
