//used to set an input field's value typically after invalid_input.js has been loaded
function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		
		var field = document.getElementById(thisScript.getAttribute('data-field'));
		var value = thisScript.getAttribute('data-value'); 

		field.value = value;
	}
}
load();
