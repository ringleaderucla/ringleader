/*
handles add/edit/delete team members for registration page 2
handles the click events for the 3 buttons at the bottom of the registration page that navigates between the carousel pages
*/
var weddingTeam = [];
var milestoneStates = [];
function editTeamMember(index) {
	if (weddingTeam[index].fullName) {
		document.getElementById('fullName').value = weddingTeam[index].fullName;
	}
	else {
		//need to get firstName and lastName since its a User model object
		document.getElementById('fullName').value = weddingTeam[index].firstName + " " + weddingTeam[index].lastName;
	}
	document.getElementById('email').value = weddingTeam[index].email;
	document.getElementById('role').value = weddingTeam[index].role;
	document.getElementById('add-user-btn').value = index;
	document.getElementById('add-user-btn').innerHTML = "Update";
	displayTeamMembers();
}
function removeTeamMember(index) {
	if (weddingTeam[index].fullName) { 
		weddingTeam.splice(index, 1);
	}
	else {
		//already exists in database so mark it for deletion instead of removing from array
		weddingTeam[index].willDelete = true;
	}
	displayTeamMembers();
}
function displayTeamMembers() {
	weddingTeamList = document.getElementById("wedding-team");
	weddingTeamList.innerHTML = "";
	for (var i = 0; i < weddingTeam.length; i++) {
		if (!weddingTeam[i].willDelete) {
			var editEntry = '<a onClick="editTeamMember(' + i.toString() + ')" href="#" ><img height="25px" src="/images/edit_btn.png"/></a>';
			var deleteEntry = '<a onClick="removeTeamMember(' + i.toString() + ')" href="#" ><img height="25px" src="/images/close_btn.png"/></a>';
			weddingTeamList.innerHTML += '<tr>'
											+ (weddingTeam[i].fullName ? weddingTeam[i].fullName
												: weddingTeam[i].firstName + '&nbsp;'
												+ weddingTeam[i].lastName) 
											+ '&nbsp;'
											+ editEntry + '&nbsp;'
											+ deleteEntry + '&nbsp;'
											+ '<hr class="separator"></hr>'
										+ '</tr>';
		}
	}
}
function milestoneBtnsSetup() {
	var milestoneBtns = $(".milestone-btn");
    for (var i = 0; i < milestoneBtns.length; i++){
		if (i%3 === 0)
			$(milestoneBtns[i]).show();
		else
			$(milestoneBtns[i]).hide();
    }

	for (var i = 0; i < milestoneBtns.length / 3; i++) {
		milestoneStates.push(0);
	}
    
    for (var i = 0; i < milestoneBtns.length; i++){
		(function(index){
			milestoneBtns[i].onclick = function(){
				//there are 3 states per button
				//this function will transition states cyclically
				var stateIndex = Math.floor((index/3));
	
				if (milestoneStates[stateIndex] === 0){
					$(milestoneBtns[stateIndex*3]).hide();
					$(milestoneBtns[stateIndex*3+1]).show();
					$(milestoneBtns[stateIndex*3+2]).hide();
					milestoneStates[stateIndex] = 1;
				}
				else if (milestoneStates[stateIndex] === 1){
					$(milestoneBtns[stateIndex*3]).hide();
					$(milestoneBtns[stateIndex*3+1]).hide();
					$(milestoneBtns[stateIndex*3+2]).show();
					milestoneStates[stateIndex] = 2;
				}
				else if (milestoneStates[stateIndex] === 2){
					$(milestoneBtns[stateIndex*3]).show();
					$(milestoneBtns[stateIndex*3+1]).hide();
					$(milestoneBtns[stateIndex*3+2]).hide();
					milestoneStates[stateIndex] = 0;
				}
			}
		})(i);
    }
}
function switchButton(btnClass, event, buttons, next, allSet, step) {
	if (event && event.target) {
		for (var i = 0; i < buttons.length; i++) {
			buttons[i].className = btnClass;		
		}
		event.target.className = btnClass + " rl-bg-green";
		step.innerHTML = event.target.value;
	}
	else {
		//make sure we're transitioning from current active slide (prevents spam clicking)
		if (buttons[$('.item.active').index()].className !== btnClass) {
			for (var i = buttons.length - 1; i > 0; i--) {
				buttons[i].className = buttons[i-1].className;		
				if (buttons[i].className !== btnClass)
					step.innerHTML = buttons[i].value;
			}
			buttons[0].className = btnClass;
		}
	}
	if (buttons[buttons.length-1].className !== btnClass) { //last step of the registration page so switch next button to all set button
		allSet.hidden = false;
		next.hidden = true;
	}
	else {
		allSet.hidden = true;
		next.hidden = false;
	}
}
function load() {
	var prevOnLoad = window.onload;
	//MUST BE LOCATED OUTSIDE OF ONLOAD
	var scripts = $('script');
	var thisScript = scripts[scripts.length - 1];
	//
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		var adminEmail = "";
		weddingTeam = JSON.parse(thisScript.getAttribute('data-team'));
		//remove admin user from list
		for (var i = 0; i < weddingTeam.length; i++) {
			if (weddingTeam[i].admin) {
				adminEmail = weddingTeam[i].email;
				weddingTeam.splice(i, 1);
				break;
			}
		}
		displayTeamMembers();
		milestoneBtnsSetup();

		var emailInUse = function() {
			document.getElementById("email-err").innerHTML = "Email already added";
		}
		var resetEmailErr = function() {
			document.getElementById("email-err").innerHTML = "";
		}
		var resetForm = function() {	
			document.getElementById('add-user-btn').innerHTML = "Add";
			document.getElementById('fullName').value = "";
			document.getElementById('email').value = "";
			resetEmailErr();
		}
		//stops the form from submitting and adds/updates local array of users which will later be sent to server
		document.getElementById('user-form').onsubmit = function() {
			var email = document.getElementById('email').value;
			var role = document.getElementById('role').value;
			if (document.getElementById('add-user-btn').innerHTML === "Add") {
				if (adminEmail == email) {
					emailInUse();
					return false;
				}
				for (var i = 0; i < weddingTeam.length; i++) {
					//dont add if email exists in the list
					if (weddingTeam[i].email == email) {
						emailInUse();
						return false;
					}
				}
				weddingTeam.push({
					fullName: document.getElementById('fullName').value,
					email: email,
					role: role,
				});
			}
			else {
				var index = parseInt(document.getElementById('add-user-btn').value);
				//email is already in use within wedding team
				for (var i = 0; i < weddingTeam.length; i++) {
					if (index != i && weddingTeam[index].email === weddingTeam[i].email) {
						emailInUse();
						resetForm();
						return false;
					}
				}
				if (weddingTeam[index].fullName) {
					weddingTeam[index].fullName = document.getElementById('fullName').value;
				}
				else {
					var separatedName = document.getElementById('fullName').value.split(" "); 
					var firstName = separatedName[0];
					var lastName = separatedName.slice(1).join(" ");
					weddingTeam[index].firstName = firstName;
					weddingTeam[index].lastName = lastName;
				}
				if (weddingTeam[index].email !== email)
					weddingTeam[index].emailChanged = true;
				weddingTeam[index].email = email;
				weddingTeam[index].role = role;
				weddingTeam[index].updated = true;
			}
			resetForm();
			displayTeamMembers();
			return false;
		}
		var request = require('request');
		document.getElementById("all-set-btn").onclick = function() {
			request({
					url: document.URL.replace(/registration.*/, "registration/complete"),
					method: "POST",
					json: {weddingTeam: weddingTeam, milestones: milestoneStates, bigDay: document.getElementById("all-set-btn").value}
				}, 
				function(err, res, body) {
					if (err) console.log(err);
					window.location = document.URL.replace(/registration.*/, "");
				});
		}

		var curStep = thisScript.getAttribute('data-step');
		if (curStep)
			curStep = parseInt(curStep) - 1;
		else
			curStep = 0;

		var next = document.getElementById('next');
		var allSet = document.getElementById('all-set');
		var step = document.getElementById('step-name');
		var buttons = new Array();
		for (var i = 0; i < 3; i++) {
			buttons.push(document.getElementById('step-btn-' + i.toString()));
		}
		var btnClass = buttons[0].className;
		//set onclick
		document.getElementById('next-btn').onclick = function() { switchButton(btnClass, null, buttons, next, allSet, step); };
		for (var i = 0; i < buttons.length; i++) {
			var index = i;
			buttons[i].addEventListener("click", function(event) { switchButton(btnClass, event, buttons, next, allSet, step); });
		}
		if (curStep < 0 || curStep >= buttons.length)
			curStep = 0;
		buttons[curStep].click();
	}
}
load();
