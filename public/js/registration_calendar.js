var DayPicker = require('react-day-picker').default;
var DateUtils = require('react-day-picker').DateUtils;

/*
Calendar for the registration date so that the user can input the wedding date
*/

var InputCalendar = React.createClass({
	getInitialState: function(){
		return {
			selectedDay: new Date(JSON.parse(this.props.weddingDay))
		};
	},
	getDate: function(day) {
		return (day.getMonth() + 1) + "/" + day.getDate() + "/" + day.getFullYear();
	},
	handleDayClick: function(e, day, modifiers) {
		this.setState({
			selectedDay: modifiers.indexOf("selected") > -1 ? null : day
		}, this.setDateDisplay);
		//set parameter for setting the wedding date
		document.getElementById("all-set-btn").value = this.getDate(day)
	},
	componentDidMount: function() {
		this.setDateDisplay();
	},
	setDateDisplay: function() {
		document.getElementById("bigDay").value = this.getDate(this.state.selectedDay);
	},
	render: function() {
		const { selectedDay } = this.state;

		return (
			<div>
				<div className="registration-calendar">
					<DayPicker
						enableOutsideDays={true}
						initialMonth={ new Date(selectedDay.getFullYear(), selectedDay.getMonth()) }
						modifiers={{
							selected: day => DateUtils.isSameDay(selectedDay, day)
						}}
						onDayClick={ this.handleDayClick }
					/>
				</div>
				<br/>
				<div style={{marginLeft: "50%", transform: "translateX(-50%)"}}>
					<div className="col-xs-offset-2 col-xs-8">
						<input style={{textAlign: "center"}} placeholder="Select a Date Above" type="text" className="form-control" id="bigDay" disabled/>
					</div>
				</div>
			</div>
		);
	}
});

var mountNode = document.getElementById('registration-calendar');

ReactDOM.render(
	<InputCalendar weddingDay={mountNode.getAttribute("data-weddingday")} />,
	mountNode
);

