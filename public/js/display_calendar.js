var DayPicker = require('react-day-picker').default;
var DateUtils = require('react-day-picker').DateUtils;

/*
Used in the Calendar page to display all the milestones and tasks that are due
*/
var DisplayCalendar = React.createClass({
	getInitialState: function(){
		var milestonesArray = JSON.parse(this.props.milestones);
		var tasksArray = JSON.parse(this.props.tasks);

		var events = [];

		// add milestones to events array
		for (var i=0; i < milestonesArray.length; i++){
			events.push({
				style: {color: '#FF0000'},
				name: milestonesArray[i].name,
				dueDate: new Date(milestonesArray[i].dueDate)
			});
		}

		// add tasks to events array
		for (var i=0; i < tasksArray.length; i++){
			events.push({
				style: {color: '#33AA33'},
				name: tasksArray[i].name,
				dueDate: new Date(tasksArray[i].dueDate)
			});
		}

		return {
			selectedDay: null,
			events: events
		};
	},

	handleDayClick: function(e, day, modifiers) {
		this.setState({
			selectedDay: modifiers.indexOf("selected") > -1 ? null : day
		});
	},

	renderDay(day){
		const date = day.getDate();

		var events = this.convertEventsToCalendarEvents(day.getMonth(), day.getFullYear(), this.state.events);

		return (
			<div>
				{date}
				<div className="Events-List">
					{events[date] && events[date].map((event, i) =>
						<div key={i} style={event.style}>
							{event.name}
						</div>
					)}
				</div>
			</div>
		);
	},

	convertEventsToCalendarEvents: function(month, year, events) {
		// extract events of current month
		var currentMonthEvents = [];
		for (var i=0; i < events.length; i++)
			if (month === events[i].dueDate.getMonth() && year === events[i].dueDate.getFullYear())
				currentMonthEvents.push(events[i]);

		var monthEventsByDay = {};
		for (var i=0; i < currentMonthEvents.length; i++){
			if (!(currentMonthEvents[i].dueDate.getDate() in monthEventsByDay))
				monthEventsByDay[currentMonthEvents[i].dueDate.getDate()] = [];
			monthEventsByDay[currentMonthEvents[i].dueDate.getDate()].push(currentMonthEvents[i]);
		}

		return monthEventsByDay;
	},
	
	componentDidMount: function() {
		window.onresize();
	},

	render: function() {
		var { selectedDay } = this.state;

		if (selectedDay === null)
			selectedDay = new Date();

		return (
			<div>
				<div className="display-calendar">
					<DayPicker
						enableOutsideDays={true}
						modifiers={{
							selected: day => DateUtils.isSameDay(selectedDay, day)
						}}
						renderDay={this.renderDay}
						onDayClick={ this.handleDayClick }
					/>
				</div>
			</div>
		);
	}
});

var mountNode = document.getElementById('display-calendar');

ReactDOM.render(
	<DisplayCalendar 
		milestones={mountNode.getAttribute("data-milestones")} 
		tasks={mountNode.getAttribute("data-tasks")} />,
	mountNode
);

