/*
Sets the position of the timeline elements
*/
function load() {
	var prevOnLoad = window.onload;
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		var timeline = document.getElementById('timeline');
		var li = timeline.getElementsByTagName('li');
		for (var i = 0; i < li.length; i++) {
			li[i].style.left = li[i].value + "%";
		}
	}
}
load();
