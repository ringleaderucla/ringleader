/*
Shows or hides the password based on checkbox
*/

function togglePassword() {
	var password = document.getElementById("password");
	if (password.type == "password")
		password.type = "text";
	else
		password.type = "password";
}

function load() {
	var prevOnLoad = window.onload;
	window.onload = function() {
		if (prevOnLoad)
			prevOnLoad();
		//shows/hides password
		document.getElementById('show-pwd').onchange = function() { togglePassword() };
	}
}
load();

