sudo apt-get -y install nodejs
sudo apt-get -y install node
sudo apt-get -y install npm
sudo apt-get -y install mongodb
#if which returns wrong path you may need to manually look for it
#try /usr/bin/npm
NPM_LOC="$(which npm)"
sudo $NPM_LOC update
sudo $NPM_LOC upgrade
sudo $NPM_LOC install -g npm
sudo $NPM_LOC install -g n
sudo $NPM_LOC install -g node-gyp
sudo $NPM_LOC install -g browserify
N_LOC="$(which n)"
sudo $N_LOC stable

#installs all the dependencies in package.json
sudo $NPM_LOC install

#NO NEED TO PUT INDIVIDUAL INSTALLS JUST MAKE SURE package.json CONTAINS ANY NEW MODULES YOU ARE USING
#To remove a module run the following:
#sudo npm uninstall --save module
#The --save will ensure the module is removed from the dependencies in package.json