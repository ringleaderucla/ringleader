## Setup ##
### Linux ###
* run `setup.sh` to install nodejs, npm, react, bootstrap and other libraries necessary to run the application
* You can run `sudo npm install` to install dependencies specified in package.json
* Make sure to regularly update your OS with `sudo apt-get update; sudo apt-get -y upgrade`

* `dev.sh` will start the server in development mode and in port 3000
    * go to browser and type in localhost:3000 to see the webpage
    * the script will also start a mongodb server

* `prod.sh` will start the server in production mode and in port 80
    * the script will also start a mongodb server

* `mongo.sh` will open up a MongoDB shell and connect to the ringleader database
	* Here you can load javascript files that can seed the database
	* You can query the database for results
	* etc.
	
### Client Rendered Views ###
We integrate ReactJS with NodeJS and so we perform server rendering for all views. This is a big performance benefit but at the same time we lose the ability to handle events (i.e. `onClick`, `onChange`, etc.). In order to create client side rendered views, it must be placed as a script inside a server rendered view. The `master.jsx` layout doesn't contain necessary scripts for client side rendering so you must use `client_render.jsx` which uses `master.jsx` but includes the necessary react, babel, etc. scripts for client side rendering. Please read the next section Browserify for more details on creating client rendered React views.

### Browserify ###
* run setup.sh or sudo npm install -g browserify
* put what you want to require in main.js 
```javascript
window.require = require
var DayPicker = require('react-day-picker').default;
var DateUtils = require('react-day-picker').DateUtils;
```
* in order to require them in our public js files make sure `window.require = require` is in the file
* run the next command in terminal (you can name the output file whatever you want)
* `browserify main.js > someName.js`
* Move the output file to public/js/libs
* Then in the server jsx view file include the next tag before the script you want to use it in
* `<script src="/js/libs/someName.js"/>`
```html
<!-- /views/registration/registration.jsx -->
<ClientRenderLayout props={this.props} noNav='true'>
    <!-- code -->
    <div id="input-calendar"></div> <!-- element for ReactDOM to render onto -->
    <script src="/js/libs/daypicker.js"></script> <!-- library to be used in input_calendar.js -->
    <script type="text/babel" src="/js/input_calendar.js"></script> <!-- client rendered React script -->
    <!-- code -->
</ClientRenderLayout>
```
* Then in the script you want to use the libraries rewrite the require statements
```javascript
/* /js/input_calendar.js */
var DayPicker = require('react-day-picker').default;
var DateUtils = require('react-day-picker').DateUtils;
var InputCalendar = React.createClass({
    ////
    // other functions
    ////
    render: function() {
        //rendering code
    }
});
ReactDOM.render(
	<InputCalendar />,
	document.getElementById('input-calendar')
);
```
* DONE!