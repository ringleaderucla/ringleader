var environment =  process.env.NODE_ENV || 'development';

var port = process.env.PORT || 3000;

var config = {
	development: {
		port: port,
		domain: 'localhost:' + port
	},
	production: {
		port: 80,
		domain: '13.93.223.125'
	}
};
var curConfig = config[environment];
var sharedAttr = {
	appName: "Wedding Planner",
	environment: environment,
	mongodb: 'mongodb://localhost:27016/ringleader',
	email: 'noreply.ringleader@gmail.com',
	mail_password: 'ringleader1',
};
sharedAttr.emailVerification = {
	verificationURL: 'http://' + curConfig.domain + '/user/email-verification/${URL}',
	//length of url hash token to generate, default is 48
	URLLength: 48,
	//length before validation link expires, default is 86400 (24 hours)
	expirationTime: 600, //10 minutes
	//settings of sender, needs to be a valid email account
	transportOptions: {
		service: 'Gmail',
		auth: {
			user: sharedAttr.email,
			pass: sharedAttr.mail_password
		}
	},
	//email settings
	verifyMailOptions: {
		from: 'Do Not Reply <' + sharedAttr.email + '>',
		subject: 'Please confirm your new ' + sharedAttr.appName + ' account',
		html: 'Click the following link to confirm your account:</p><p>${URL}</p>',
		text: 'Please confirm your account by clicking the following link: ${URL}'
	}
}
sharedAttr.mailer = {
	from: 'Do Not Reply <' + sharedAttr.email + '>',
	host: 'smtp.gmail.com', // hostname 
	secureConnection: true, // use SSL 
	port: 465, // port for secure SMTP 
	transportMethod: 'SMTP', // default is SMTP. Accepts anything that nodemailer accepts 
	auth: {
		user: sharedAttr.email,
		pass: sharedAttr.mail_password
	}
}

//copies sharedAttr to the config enviroment that will be used
for (var attr in sharedAttr) { curConfig[attr] = sharedAttr[attr]; }

exports = module.exports = curConfig;
