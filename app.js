var express = require('express');
var path = require('path');
var fs = require('fs');
var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var app = express();
var nev = require('email-verification')(mongoose);
const assert = require('assert');
module.exports = { 
	app: app, 
	nev: nev
};
var config = require('./config');
console.log("Environment: " + config.environment);

// set port number to use
app.set('port', config.port);

app.use(express.static(__dirname + '/public', { maxAge: '14d' }));


/////////////
// view engine setup
app.set('views', __dirname + '/views');
app.set('view engine', 'jsx');
app.engine('jsx', require('express-react-views').createEngine());
/////////////


/////////////
// Use application-level middleware for common functionality
var bodyParser = require('body-parser')
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(require('morgan')('combined'));
app.use(require('cookie-parser')());
var session = require('express-session');
var MongoDBStore = require('connect-mongodb-session')(session);
var store = new MongoDBStore(
  { 
    uri: config.mongodb,
    collection: 'sessions'
  });

// Catch errors 
store.on('error', function(error) {
  assert.ifError(error);
  assert.ok(false);
});
app.use(session({ 
	secret: 'cs130 ringleader', 
	cookie: {
    	maxAge: 1000 * 60 * 60 * 24 * 7 // 1 week 
      },
	store: store,
	resave: false, 
	saveUninitialized: false }));
/////////////


/////////////
// user login system setup
// Initialize Passport and restore authentication state, if any, from the
// session.
app.use(passport.initialize());
app.use(passport.session());
// requires the model with Passport-Local Mongoose plugged in
var User = require('./models/user');

passport.use(User.createStrategy());

// use static serialize and deserialize of model for passport session support
passport.serializeUser(User.serializeUser());
passport.deserializeUser(User.deserializeUser());

/////////////


/////////////
// database setup
mongoose.connect(config.mongodb);
var User = require('./models/user');
var Wedding = require('./models/wedding');
var Milestone = require('./models/milestone');
var Task = require('./models/task');
if ('development' == config.environment) {
	
	function randomDate(start, end) {
		return new Date(start.getTime() + Math.random() * (end.getTime() - start.getTime()));
	}
	
	//random database entries for testing purposes

	//initialize wedding
	function initWedding(cb) {
		var wedding = new Wedding({ registered: true, bigDay: new Date('10/20/17') });
		wedding.save(function(err, wedding) {
			cb(err, wedding);
		});
	}

	//initialize users
	function initUsers(wedding, cb) {
		var roles = User.schema.path('role').enumValues;
		var users = [
			{firstName: 'John', lastName: 'Doe', email: 'johndoe@example.com', wedding: wedding._id, role: roles[1], admin: false },
			{firstName: 'Jane', lastName: 'Doe', email: 'janedoe@example.com', wedding: wedding._id, role: roles[0], admin: true },
			{firstName: 'Helper', lastName: 'One', email: 'h1@example.com', wedding: wedding._id, role: roles[1], admin: false },
			{firstName: 'Helper', lastName: 'Two', email: 'h2@example.com', wedding: wedding._id, role: roles[1], admin: false },
			{firstName: 'Participant', lastName: 'One', email: 'p1@example.com', wedding: wedding._id, role: roles[2], admin: false }
		];
		var usersFinal = new Array();
		for (var i = 0; i < users.length; i++) {
			var user = new User(users[i]);
			usersFinal.push(user);
			if (i == users.length - 1) {
				User.register(user, "tester", function(err, user) {
					if (err) console.log(err);
					if (user) 
						User.verifyEmail(user.authToken, function(err) { 
							cb(err, usersFinal);
						});
				});
			}
			else {
				User.register(user, "tester", function(err, user) {
					if (err) console.log(err);
					if (user) 
						User.verifyEmail(user.authToken, function(err) { 
							if (err) console.log(err); 
						});
				});
			}
		}
	}

	//initialize milestones
	function initMilestones(wedding, users, cb) {
		var events = [
			{ wedding: wedding._id, name: 'Invitations', description: 'Send invitations', dueDate: new Date('6/5/16') },
			{ wedding: wedding._id, name: 'Wedding Dress', description: 'Get wedding dress', dueDate: new Date('10/15/16') },
			{ wedding: wedding._id, name: 'Flowers', description: 'Lilies', dueDate: new Date('12/25/16') },
			{ wedding: wedding._id, name: 'Wedding Hall', description: 'Book wedding hall', dueDate: new Date('1/2/17') },
			{ wedding: wedding._id, name: 'Dinner Rehearsal', description: 'Host dinner rehearsal', dueDate: new Date('3/5/17') },
		];
		var eventsFinal = new Array();
		for (var i = 0; i < events.length; i++) {
			var user = users[i % users.length];
			events[i].dueDate.setHours(0,0,0,0);
			events[i].personInCharge = user;
			events[i].createdBy = users[(i % users.length == 0 ? users.length : i % users.length)-1];
			var newEvent = new Milestone(events[i]);

			// Save milestone in DB
			if (i == events.length - 1) {
				newEvent.save(function(err, milestone) {
					cb(err, eventsFinal);
				});
			}
			else {
				newEvent.save();
			}

			eventsFinal.push(newEvent);
		}
	}

	//initialize tasks
	function initTasks(wedding, users, milestones) {
		for (var i = 0; i < milestones.length; i++) {
			for (var j = 0; j < 6; j++) {
				var date = randomDate(new Date(), milestones[i].dueDate);
				date.setHours(0,0,0,0);
				var newEvent = new Task({
					wedding: wedding._id,
					name: "Task " + (j * 100) + i,
					description: "Another random task",
					createdBy: users[Math.round(Math.random() * users.length)],
					personInCharge: users[Math.round(Math.random() * users.length)],
					parentMilestone: milestones[i],
					dueDate: date
				});

				// Save task in DB
				newEvent.save();
			}
		}
	}
	

	//remove all
	Wedding.remove({}, function(err) {
		if (err)
			console.log(err);
		Task.remove({}, function(err) {
			if (err)
				console.log(err);
			Milestone.remove({}, function(err) {
				if (err)
					console.log(err);
				User.remove({}, function(err) {
					if (err)
						console.log(err);
					initWedding(function(err, wedding) {
						if (err) console.log(err);
						initUsers(wedding, function(err, users) {
							if (err) console.log(err);
							initMilestones(wedding, users, function(err, milestones) {
								if (err) console.log(err);
								initTasks(wedding, users, milestones);
							});
						});
					});
				});
			});
		});
	});
}
/////////////


/////////////
// set up mailer
var mailer = require('express-mailer');
mailer.extend(app, config.mailer);

//the model that will be verified
config.emailVerification.persistentUserModel = User;
nev.configure(config.emailVerification);
//end email verification settings
nev.generateTempUserModel(User);
/////////////
 

/////////////
// automatically uses all the routes defined in the routes folder
files = fs.readdirSync(process.cwd() + '/routes');
for (var i = 0; i < files.length; i++) {
	if (files[i].indexOf('.js') + 3 != files[i].length)
		continue;
	files[i] = files[i].replace('.js', '');
	var controller = require('./routes/' + files[i]);
	console.log('Using route file: ' + files[i]);
	if (files[i] === 'index') // index should not be part of url so change it to empty string
		files[i] = '';
	app.use('/' + files[i], controller);
}
/////////////


var server = app.listen(app.get('port'), function() {
	console.log("App Started with port " + app.get('port'));
});
