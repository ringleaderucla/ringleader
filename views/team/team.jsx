var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var EmailLayout = require('../layout/email');
var RoleLayout = require('../layout/role');
var FullNameLayout = require('../layout/fullname'); 

var page = function() { 

var invite = null;
var editDeleteHeader = null;
var entries = new Array();
var team = this.props.team;
var addEntry = function(i, editEntry, deleteEntry) {
	var object = team[i];
	entries.push(<tr key={i}>
					<td>{object.firstName}</td>
					<td>{object.lastName}</td>
					<td>{object.email}</td>
					<td>{object.role}</td>
					{editEntry}
					{deleteEntry}
				</tr>);
}
if (this.props.user.admin) {
	//only admins can add, edit and delete helpers and admins cannot remove admins
	var btnStyle = {	
		minWidth: '120px',
		minHeight: '40px'
	};
	invite = <div className="row">
				{ this.props.invalidEmail ? <script src="/js/invalid_input.js" data-valid-fields='["fullName"]' data-invalid-field="email" data-err-msg="Email is already registered" /> : null }
				<div className="col-sm-12">
				<form className="form-horizontal" role="form" method="post" action="/team/add" encType="application/json">
					<div className="form-group">
						<FullNameLayout group=" " inputCol="col-sm-2" inputErr="Please supply first and last name" />
						<EmailLayout group=" " labelCol="col-sm-1" inputCol="col-sm-2"/>
						<RoleLayout group=" " labelCol="col-sm-1" inputCol="col-sm-2" roles={this.props.roles} />
						<div className="col-sm-2 text-center" >
							<button type="submit" id="submit" style={btnStyle} className="btn-rl btn-pad-small btn-vertical-center no-margin rl-bg-orange" >Add</button>
						</div>
					</div>
				</form>
				</div>
			</div>;
	editDeleteHeader = <th></th>;
	for (var i = 0; i < team.length; i++) {
		var object = team[i];
		var editEntry = <td className="td-link"><a href={"/team/edit?id="+object._id}><img height="25px" src="/images/edit_btn.png"/></a></td>;
		var deleteEntry = <td className="td-link"><a href={"/team/delete?id="+object._id}><img height="25px" src="/images/close_btn.png"/></a></td>;
		if (object.admin)
			deleteEntry = <td></td>;
		addEntry(i, editEntry, deleteEntry);
	}
}
else {
	for (var i = 0; i < team.length; i++) {
		addEntry(i, null, null);
	}
}
return (
	<DefaultLayout props={this.props}>
		<div className="container">
			<h1>Team</h1>      
			{invite}
			<table data-toggle="table" data-sort-name="last name" data-sort-order="asc" 
				className="table table-striped table-bordered table-dark-border">
				<thead>
					<tr>
						<th data-sortable="true">First Name</th>
						<th data-field="last name" data-sortable="true">Last Name</th>
						<th data-sortable="true">Email</th>
						<th data-sortable="true">Role</th>
						{editDeleteHeader}
						{editDeleteHeader}
					</tr>
				</thead>
				<tbody>
					{
						entries
					}
				</tbody>
			</table>
		</div>
    </DefaultLayout>
)};

var TeamComponent = React.createClass({
    render: page
});

module.exports = TeamComponent;
