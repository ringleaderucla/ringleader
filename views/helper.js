var React = require('react');
module.exports = {
	tableSeeAllDescription: function(Object) {
		if (Object && Object.description) {
			var newLineIndex = Object.description.indexOf("\n") - 1;
			var endIndex = 40;
			if (newLineIndex > 0 && newLineIndex < endIndex)
				endIndex = newLineIndex;
			var displayedDescription = Object.description.substring(0, endIndex);
			//only show See All popover if the description is too long
			return (
				<td className="col-sm-4">
					{displayedDescription + " "}
					{Object.description.length > displayedDescription.length ? 
						<span>
							...&nbsp;
							<a href="#" data-toggle="popover" data-placement="left" title={Object.name} data-trigger="focus" data-html="true" 
								data-content={Object.description.replace(/(?:\r\n|\r|\n)/g, '<br />')}>
								<strong>See All</strong>
							</a>
						</span>
						: null
					}
				</td>
			)
		}
		return <td></td>;
	}
}
