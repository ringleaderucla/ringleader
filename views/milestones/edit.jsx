var React = require('react');
var ClientRenderLayout = require('../layout/client_render'); 
var MilestoneFormLayout = require('./form'); 

var page = function() { 
return (
    <ClientRenderLayout props={this.props}>
		<div className="container">
			<h2>Edit Milestone</h2>
			<div className="text-right">
				<a href="/milestones" type="button" className="btn-rl btn-pad-xsmall rl-bg-orange" >Cancel</a>
			</div>
			<br/>
			<MilestoneFormLayout props={this.props} />
        </div>
    </ClientRenderLayout>
)};

var EditMilestoneComponent = React.createClass({
    render: page
});

module.exports = EditMilestoneComponent;
 
 
