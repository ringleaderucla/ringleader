var React = require('react');
var dateFormat = require('dateformat');
var InputLayout = require('../layout/form_input'); 
var SelectLayout = require('../layout/form_select'); 
var TextAreaLayout = require('../layout/textarea'); 

var MilestoneFormComponent = React.createClass({
    render: function() {
		var team = new Array();
		var leadIndex = -1;
		if (this.props.add) {
			leadIndex--;
		}
		for (var i = 0; i < this.props.props.team.length; i++) {
			if (leadIndex == -1 && this.props.props.team[i]._id.toString() == this.props.props.milestoneToEdit.personInCharge.toString())
				leadIndex = i;
			team.push(this.props.props.team[i].firstName + " " + this.props.props.team[i].lastName + " (" + this.props.props.team[i].email + ")");
		}
		return (
		<form id="milestone-form" className="form" role="form" action={"/milestones/" + (this.props.add ? "add" : "edit?id=" + this.props.props.milestoneToEdit._id)} method="post" encType="application/json">
			<div className="col-sm-6">
				{ this.props.duplicate ? <script src="/js/invalid_input.js" data-valid-fields='[]' data-invalid-field="name" data-err-msg="A milestone with that name already exists on that date" /> : null }
				<InputLayout value={this.props.add ? "" : this.props.props.milestoneToEdit.name} id="name" label="Name" inputErr="Please supply a name for the milestone" type="text" placeholder="Enter Milestone Name" />
				{leadIndex > 0 ? <script src="/js/select_option.js" data-select-id="lead" data-option={leadIndex} /> : null }
				<SelectLayout id="lead" label="Lead Contact" options={team} />
				<div id="input-calendar" data-date={this.props.add ? "" : this.props.props.milestoneToEdit.dueDate} data-label="Due Date" data-bad-date={this.props.props.badDate} data-max-date={dateFormat(this.props.props.user.wedding.bigDay, "mm/dd/yyyy")}></div>
				<link href='/css/react_input_calendar.css' rel='stylesheet' type='text/css' />
				<script src="/js/libs/input_calendar.js" />
				<script src="/js/input_calendar.js" type="text/babel" />
			</div>
			<div className="col-sm-6">
				<TextAreaLayout value={this.props.add ? "" : this.props.props.milestoneToEdit.description} id="description" label="Description" rows="10" inputErr="Please supply a description for the milestone" placeholder="Enter Description" />
			</div>
			<div className="form-group text-left">
				<input type="submit" value="Done" className="btn-rl btn-pad-xsmall rl-bg-orange" />
			</div>
		</form> 
    )}
});

module.exports = MilestoneFormComponent;

