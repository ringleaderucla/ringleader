var React = require('react');
var dateFormat = require('dateformat');
var DefaultLayout = require('../layout/master'); // import the master layout
var TimelineLayout = require('../layout/timeline');
var helper = require('../helper.js');

var page = function() { 
var user = this.props.user;
    return (
        <DefaultLayout props={this.props}>
            <div className="container">
                <h1>Milestones</h1>

				<TimelineLayout user={this.props.user} milestones={this.props.milestones}/>

				<table data-toggle="table" data-sort-name="date" data-sort-order="asc" 
					className="table table-striped table-bordered table-dark-border">
                    <thead>
                        <tr>
                            <th data-sortable="true">Event</th>
                            <th data-sortable="true">Lead Contact</th>
                            <th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="dateSorter">Date</th>
                            <th>Description</th>
							<th data-sortable="true">Status</th>
							<th></th>
							<th></th>
                        </tr>
                    </thead>
                    <tbody>
                        {
                            // output the table based on routes/milestones props 'milestones'
                            this.props.milestones.map(function(Object, i) {
                                function getEvent() {
                                    return Object.name;
                                };

                                // check that the user has the correct necessary inputs
                                function getLead() {
                                    var lead;
                                    if (Object.personInCharge != null) {
                                        if (Object.personInCharge.firstName != null) {
                                            lead = Object.personInCharge.firstName;
                                        }
                                        if (Object.personInCharge.lastName != null) {
                                            lead = lead.concat(" ", Object.personInCharge.lastName);
                                        }
                                    } else {
                                        lead = "";
                                    }
                                    return lead;
                                };

                                // output the date as "Month ##th, Year"
                                function getDate() {
                                    return dateFormat(Object.dueDate, "mmmm dS, yyyy");
                                };
                                
								function getEditEntry(){
									if (user.admin || Object.createdBy.toString() === user.id.toString())
										return <td className="td-link"><a href={"/milestones/edit?id="+Object._id}><img height="25px" src="/images/edit_btn.png"/></a></td>;
									else
										return <td></td>;
								}
								
								function getDeleteEntry(){ 
									if (user.admin || Object.createdBy.toString() === user.id.toString())
										return <td className="td-link"><a href={"/milestones/delete?id="+Object._id}><img height="25px" src="/images/close_btn.png"/></a></td>;
									else
										return <td></td>;
								}
								function getStatus() {
									if (user.admin || Object.personInCharge._id.toString() === user.id.toString() || Object.createdBy.toString() === user.id.toString())
										return <td className="td-link"><a className={"btn-rl status-link " + (Object.status ? "rl-bg-green" : "rl-bg-red")} value={Object._id} href={"/milestones/status?id=" + Object._id} >{Object.status ? "Done!" : "Not Done"}</a></td>
									else
										return <td>{Object.status ? "Done!" : "Not Done"}</td>
	
								}
                                return (
                                    <tr className="milestones" key={i}>
										<td>{getEvent()}</td>
										<td>{getLead()}</td>
										<td data-time={Object.dueDate.getTime()}>{getDate()}</td>
										{helper.tableSeeAllDescription(Object)}
										{getStatus()}
										{getEditEntry()}
										{getDeleteEntry()}
                                    </tr>
                                )
                            })
                        }
                    </tbody>
                </table>
				<br/>
                <a href="milestones/add" className="btn-rl btn-pad-xsmall rl-bg-orange">Add New Milestone</a>
            </div>
        </DefaultLayout>
    )
};

var MilestonesComponent = React.createClass({
    render: page
});

module.exports = MilestonesComponent;
