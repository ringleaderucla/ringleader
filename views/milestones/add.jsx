var React = require('react');
var ClientRenderLayout = require('../layout/client_render'); 
var MilestoneFormLayout = require('./form'); 

var page = function() { 
return (
    <ClientRenderLayout props={this.props}>
		<div className="container">
			<h2>Add Milestone</h2>
			<div className="text-right">
				<a href="/milestones" type="button" className="btn-rl btn-pad-xsmall rl-bg-orange" >Cancel</a>
			</div>
			<br/>
			<MilestoneFormLayout add="true" props={this.props} />
        </div>
    </ClientRenderLayout>
)};

var AddMilestoneComponent = React.createClass({
    render: page
});

module.exports = AddMilestoneComponent;
