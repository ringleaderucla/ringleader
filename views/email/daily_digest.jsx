 var React = require('react');
var config = require('../../config');

var page = function() { 
var url = "http://" + this.props.url;
return (
	<body>
		<p>Hi {this.props.name},</p>
		<p>
			This is a daily digest email from {config.appName} to remind you that you have:
		</p>
		<p>{this.props.updated.length} milestones and tasks were updated today</p>
		<ul>
			{
				this.props.updated.map(function(Object, i) {
					return (
						<li key={i}>{Object}</li>
					)
				})
			}
		</ul>
		<p>{this.props.dueSoon.length} milestones and tasks are due in a week</p>
		<ul>
			{
				this.props.dueSoon.map(function(Object, i) {
					return (
						<li key={i}>{Object}</li>
					)
				})
			}
		</ul>
		<p>{this.props.overdue.length} milestones and tasks are overdue</p>
		<ul>
			{
				this.props.overdue.map(function(Object, i) {
					return (
						<li key={i}>{Object}</li>
					)
				})
			}
		</ul>
		<p>Please log in to your {config.appName} account at {url} to see more detail. If there are any milestones and tasks that are complete please mark them as complete so that the rest of your wedding planning team is informed!</p>
	</body>
)};

var Email = React.createClass({
    render: page
});

module.exports = Email; 
