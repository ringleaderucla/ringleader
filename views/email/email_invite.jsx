 var React = require('react');

var page = function() { 
var url = "http://" + this.props.url;
return (
	<body>
		<p>Click the link to login to your account</p>
		<a href={url} >{url}</a>
		<p>You're password is {this.props.password}</p>
	</body>
)};

var Email = React.createClass({
    render: page
});

module.exports = Email;