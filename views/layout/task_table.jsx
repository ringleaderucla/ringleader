var React = require('react');
var dateFormat = require('dateformat');
var helper = require('../helper.js');

var page = function() { 
var user = this.props.user;
var home = this.props.home;
return (
	<table data-toggle="table" data-sort-name="date" data-sort-order="asc" 
		className="table table-striped table-bordered table-dark-border">
		<thead>
			{ this.props.tasks ? 
				<tr>
					<th data-sortable="true">Task</th>
					<th data-sortable="true">Lead Contact</th>
					<th data-sortable="true">Milestone</th>
					<th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="dateSorter">Date</th>
					{this.props.home ? null : <th>Description</th>}
					<th data-sortable="true">Status</th>
					{this.props.home ? null : <th></th> }
					{this.props.home ? null : <th></th> }
				</tr>
				:
				<tr>
					<th data-sortable="true">Task</th>
					<th>Update Log</th>
					<th data-sortable="true">Milestone</th>
					<th data-field="date" data-sortable="true" data-sort-name="_date_data" data-sorter="dateSorter">Update Date</th>
					<th data-sortable="true">Status</th>
				</tr>
			}
		</thead>
		<tbody> 
			{ this.props.tasks ? 
				(this.props.tasks.map(function(Object, i) {
					function getEditEntry(){
						if (user.admin || Object.createdBy.toString() === user.id.toString())
							return <td className="td-link"><a href={"/tasks/edit?id="+Object._id}><img height="25px" src="/images/edit_btn.png"/></a></td>;
						else
							return <td></td>;
					}
					function getDeleteEntry(){ 
						if (user.admin || Object.createdBy.toString() === user.id.toString())
							return <td className="td-link"><a href={"/tasks/delete?id="+Object._id}><img height="25px" src="/images/close_btn.png"/></a></td>;
						else
							return <td></td>;
					}
					function getStatus() {
						if (user.admin || Object.personInCharge._id.toString() === user.id.toString() || Object.createdBy.toString() === user.id.toString())
							return <td className="td-link"><a className={"btn-rl status-link " + (Object.status ? "rl-bg-green" : "rl-bg-red")} value={Object._id} href={"/tasks/status?id=" + Object._id + (home ? "&home=true" : "")} >{Object.status ? "Done!" : "Not Done"}</a></td>
						else
							return <td>{Object.status ? "Done!" : "Not Done"}</td>
	
					}
					return(
						<tr key={i}>
							<td>{Object.name}</td>
							<td>{Object.personInCharge ? Object.personInCharge.firstName + " " + Object.personInCharge.lastName : ""}</td>
							<td>{Object.parentMilestone ? Object.parentMilestone.name : ""}</td>
							<td data-time={Object.dueDate.getTime()}>{dateFormat(Object.dueDate, "mmmm dS, yyyy")}</td>
							{home ? null : helper.tableSeeAllDescription(Object)}
							{getStatus()}
							{home ? null : getEditEntry()}
							{home ? null : getDeleteEntry()}
						</tr>
					)
				}))
				:
				(this.props.updatedTasks.map(function(Object, i) {
					return(
						<tr key={i}>
							<td>{Object.name}</td>
							<td className="col-sm-4">{Object.updateDescription}</td>
							<td>{Object.parentMilestone ? Object.parentMilestone.name : ""}</td>
							<td>{dateFormat(Object.updated_at, "mmmm dS, yyyy")}</td>
							<td>{Object.status ? "Done!" : "Not Done"}</td>
						</tr>
					)
				}))
			}
		</tbody>
	</table>
)};

var TaskTableComponent = React.createClass({
    render: page
});

module.exports = TaskTableComponent;

