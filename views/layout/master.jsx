var React = require('react');
var config = require('../../config');

var page = function() { 

//show Login if user is not logged in and Logout if user is logged in
var profile = function() {};
var navbtns = function() {};
if (this.props.props.user) {
	var fullName = this.props.props.user.firstName + " " + this.props.props.user.lastName;
	var registration = <li><a href="/registration">Registration</a></li>;
	if (!this.props.props.user.admin) //only admin can register wedding
		registration = null;
	profile = function() { return (
		<li className="dropdown rl-bg-green">
			<a className="dropdown-toggle rl-bg-green" data-toggle="dropdown" href="#">{fullName}</a>
			<ul className="dropdown-menu rl-bg-green">
				<li><a href="/user/profile">Profile</a></li>
				{registration}
				<li><a href="/user/logout">Logout</a></li>
			</ul>
		</li>
	)};
	if (!this.props.noNav) {
		navbtns = function() { return (
			<ul className="nav navbar-nav">
				<li><a href="/milestones">Milestones</a></li>
				<li><a href="/tasks">To Dos</a></li>
				<li><a href="/calendar">Calendar</a></li>
				<li><a href="/team">Team</a></li>
			</ul>
		)};
	}
}

return (
<html lang="en">
	<head>
		<meta httpEquiv="Content-Type" content="text/html, charset=utf-8" />
		<meta name="viewport" content="width=device-width, initial-scale=1" />
		<title>{config.appName} - {this.props.props.name}</title>

		{/* Using Bootstrap */}
		<link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro:400,600,700' rel='stylesheet' type='text/css' />
		<link href='https://fonts.googleapis.com/css?family=Source+Serif+Pro:400,600,700' rel='stylesheet' type='text/css' />
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap.min.css"/>
		<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/latest/css/bootstrap-theme.min.css"/>
		<link href="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.css" rel="stylesheet" />
		<link rel="stylesheet" href="/css/application.css"/>
	</head>			
	<body>
		<nav id="navigation-bar" className="navbar navbar-default">
			<div className="container-fluid">
				<div className="navbar-header">
					<a href="/"><strong className="navbar-brand">{config.appName}</strong></a>
				</div>
				{ navbtns() }
				<ul className="nav navbar-nav navbar-right">
					<li><a href="http://bit.ly/ringleaderhelp">Help!</a></li>
					{ profile() }
				</ul>
			</div>
		</nav>
		<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.0/jquery.min.js"></script>
		<div id="main-body">
			{this.props.children}
		</div>
		<script src="http://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js"></script>
		<script src="https://cdnjs.cloudflare.com/ajax/libs/bootstrap-table/1.10.1/bootstrap-table.min.js"></script>
		<script src="/js/master.js"/>
	</body>
</html>
)};

var MasterLayout = React.createClass({
	render: page
});

module.exports = MasterLayout;
