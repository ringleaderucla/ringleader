var React = require('react');
var InputLayout = require('../layout/form_input');

var page = function() { 
var inputCol = this.props.inputCol ? this.props.inputCol : "col-sm-10";
var labelCol = this.props.labelCol ? this.props.labelCol : "col-sm-2";
var label = this.props.label ? this.props.label : "Email";
var disabled = this.props.disabled ? this.props.disabled : "false";
var value = this.props.value ? this.props.value : "";
return (		
	<InputLayout group={this.props.group} id="email" labelCol={labelCol} inputCol={inputCol} label={label} inputPattern="[^ ]+@[^ ]+" inputErr="Please supply an email address" type="email" placeholder="Enter Email" value={value} required="true" disabled={disabled} />	
)};

var EmailComponent = React.createClass({
    render: page
});

module.exports = EmailComponent;
