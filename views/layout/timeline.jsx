var React = require('react');
var dateFormat = require('dateformat');

const MONTH_NAMES = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"
];
const MAX_LINE_HEIGHT = 200.0;

var page = function() { 

var timelineEvents = new Array();

//Get today's date but to 0 time
var curDate = new Date();
curDate.setHours(0,0,0,0);


//Set up the months on the timeline
function addMonthToTimeline(value, monthIndex) {
	timelineEvents.push(
		<li key={timelineEvents.length} className="timeline_event_month" value={value}>
			<div className="timeline_event_text_month" value={value}>
				{MONTH_NAMES[monthIndex]}
			</div>
		</li>
	);
}
var monthlyEvents = {};
function initMonthlyEvent(month, year, lowerValue, upperValue) {
	monthlyEvents[month + 100 * year] = { lowerValue: lowerValue, upperValue: upperValue, events: new Array() };
}

function setMonthlyEvent(event) {
	var index = event.dueDate.getMonth() + 100 * event.dueDate.getFullYear();
	if (monthlyEvents[index])
		monthlyEvents[index].events.push(event);
}
var nextMonth = curDate.getMonth() + 1;
var weddingMonth = this.props.user.wedding.bigDay.getMonth();
var curYear = curDate.getFullYear();
var weddingYear = this.props.user.wedding.bigDay.getFullYear();
var years = weddingYear - curYear;
if (years == 0) { //same year
	var monthCount = weddingMonth - nextMonth + 1;
	var inc = (100.0) / (monthCount + 1);
	//go from nextMonth to weddingMonth
	for (var i = 0; i < monthCount; i++) {
		var value = (i+1) * inc;
		addMonthToTimeline(value, i + nextMonth);
		initMonthlyEvent(i + nextMonth, curYear, value, value + inc);
	}
	initMonthlyEvent(nextMonth - 1, curYear, 0, inc); //current month
}
else {
	years--;
	var yearOneMonthCount = MONTH_NAMES.length - nextMonth;
	var yearLastMonthCount = weddingMonth + 1;
	var yearsInBetweenMonthCount = MONTH_NAMES.length * years;
	var monthCount = yearOneMonthCount + yearLastMonthCount + yearsInBetweenMonthCount;
	var inc = (100.0) / (monthCount + 1);
	//go from nextMonth to December
	for (var i = 0; i < yearOneMonthCount; i++) {
		var value = (i+1) * inc;
		addMonthToTimeline(value, i + nextMonth);
		initMonthlyEvent(i + nextMonth, curYear, value, value + inc);
	}
	initMonthlyEvent(nextMonth - 1, curYear, 0, inc); //current month
	//add all the months for every year in between curYear and weddingYear
	for (var i = 0; i < years; i++) {
		for (var j = 0; j < MONTH_NAMES.length; j++) {
			var value = (i * MONTH_NAMES.length + j + yearOneMonthCount + 1) * inc;
			addMonthToTimeline(value, j);
			initMonthlyEvent(j, curYear + i + 1, value, value + inc);
		}
	}
	//go from January to weddingMonth
	for (var i = 0; i < yearLastMonthCount; i++) {
		var value = (i + yearOneMonthCount + yearsInBetweenMonthCount + 1) * inc;
		addMonthToTimeline(value, i);
		initMonthlyEvent(i, weddingYear, value, value + inc);
	}
}
			
//prune events that should not be displayed on timeline
for (var i = 0; i < this.props.milestones.length; i++) {
	if (this.props.milestones[i].dueDate >= curDate && this.props.milestones[i].dueDate <= this.props.user.wedding.bigDay) {
		setMonthlyEvent(this.props.milestones[i]);
	}
}

var lineHeightVariance = 3.0;
for (var attr in monthlyEvents) {
	if (monthlyEvents[attr].length == 0)
		continue;
	var events = monthlyEvents[attr].events;
	var lowerValue = monthlyEvents[attr].lowerValue;
	var upperValue = monthlyEvents[attr].upperValue;
	var inc = (upperValue - lowerValue) / (events.length + 1);
	for (i = 0; i < events.length; i++) {
		var lineHeight = MAX_LINE_HEIGHT * ((lineHeightVariance - Math.abs(timelineEvents.length % (2*lineHeightVariance) - lineHeightVariance)) / lineHeightVariance);
		timelineEvents.push(
			<li key={timelineEvents.length} className="timeline_event" value={(i+1) * inc + lowerValue}>
				<div className="vertical_line" style={{height: lineHeight + "px"}}></div>
				<div className="timeline_event_text_bottom" style={{marginTop: lineHeight + 40 + "px"}} value={(i+1) * inc + lowerValue}>
					<div className="row">{dateFormat(events[i].dueDate, "mm/dd/yy")}</div>
					<div className="row">{events[i].name}</div>
				</div>
			</li>);
	}
}

return (
	<div>
		<script src="/js/timeline.js"/>
		<div id="timeline" className="timeline" style={{height: MAX_LINE_HEIGHT + 30}}>
			<span className="filling_line rl-bg-purple"></span>
			<ol>
				<li className="timeline_event_big" value="0">
					<div className="timeline_event_text_top" value="0">
					Today
					</div>
				</li>
				{timelineEvents}
				<li className="timeline_event_heart" value="100">
					<div className="timeline_event_text_top" value="100">
					{dateFormat(this.props.user.wedding.bigDay, "mm/dd/yy")}
					</div>
				</li>
			</ol>
		</div>
	</div>
)
};

var TimelineLayout = React.createClass({
	render: page
});

module.exports = TimelineLayout;
 
