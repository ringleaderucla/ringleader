var React = require('react');

var page = function() {
var options = new Array();
for (var i = 0; i < this.props.options.length; i++) {
	options.push(<option key={i}>{this.props.options[i]}</option>);
}
var group = this.props.group ? this.props.group : "form-group"; 
return (		
	<div className={group}>
		<label className={"control-label " + this.props.labelCol} htmlFor={this.props.id}>{this.props.label}</label>
		<div className={this.props.inputCol}>          
			<select className="form-control" id={this.props.id} name={this.props.id} required>
				{options}
			</select>
		</div>
	</div>
)};

var SelectComponent = React.createClass({
    render: page
});

module.exports = SelectComponent;
 
