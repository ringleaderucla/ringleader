var React = require('react');

/*
var InputLayout = require('../layout/form_input');
//
<InputLayout  group="" id="" labelCol="" inputCol="" label="" inputPattern="" inputErr="" type="" placeholder="" value="" required="" disabled="" />
*/
var page = function() { 
var required = true;
if (this.props.required != null) {
	if (this.props.required == "false") {
		required = false;
	}
}
var disabled = false;
if (this.props.disabled != null) {
	if (this.props.disabled == "true") {
		disabled = true;
	}
}
var errDiv = <div className="row invalid-red" id={this.props.id + "-err"}></div>;
var group = this.props.group ? this.props.group : "form-group";
return (		
	<div className={group}>
  		<label className={"control-label " + this.props.labelCol} htmlFor={this.props.id}>{this.props.label}</label>
		<div className={this.props.inputCol}>
			<input pattern={this.props.inputPattern} title={this.props.inputErr} type={this.props.type} className="form-control" name={this.props.id} id={this.props.id} placeholder={this.props.placeholder} defaultValue={this.props.value} required={required} disabled={disabled}/>
			{this.props.children}
			{errDiv}
		</div>
		<script src="/js/set_invalid_msg.js" data-id={this.props.id} />
	</div>
)};

var InputComponent = React.createClass({
    render: page
});

module.exports = InputComponent;
