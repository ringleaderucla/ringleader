var React = require('react');

/*
var TextAreaLayout = require('../layout/textarea');
//
<TextAreaLayout  group="" id="" labelCol="" inputCol="" label="" rows="" inputErr="" placeholder="" value="" required="" disabled="" />
*/
var page = function() { 
var required = true;
if (this.props.required != null) {
	if (this.props.required == "false") {
		required = false;
	}
}
var disabled = false;
if (this.props.disabled != null) {
	if (this.props.disabled == "true") {
		disabled = true;
	}
}
var errDiv = <div className="row invalid-red" id={this.props.id + "-err"}></div>; {/*Used by Firefox*/}
var group = this.props.group ? this.props.group : "form-group";
var rows = this.props.rows ? this.props.rows : 4;
return (		
	<div className={group}>
		<script src="/js/set_invalid_msg.js" data-id={this.props.id} />
  		<label className={"control-label " + this.props.labelCol} htmlFor={this.props.id}>{this.props.label}</label>
		<div className={this.props.inputCol}>
			<textarea rows={rows} title={this.props.inputErr} className="form-control" name={this.props.id} id={this.props.id} placeholder={this.props.placeholder} value={this.props.value} required={required} disabled={disabled}/>
			{this.props.children}
			{errDiv}
		</div>
	</div>
)};

var TextAreaComponent = React.createClass({
    render: page
});

module.exports = TextAreaComponent;
