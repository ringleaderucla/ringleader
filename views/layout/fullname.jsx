var React = require('react');
var InputLayout = require('../layout/form_input');

var page = function() { 
var inputCol = this.props.inputCol ? this.props.inputCol : "col-sm-10";
var labelCol = this.props.labelCol ? this.props.labelCol : "col-sm-2";
var inputErr = this.props.inputErr ? this.props.inputErr : "Please supply your first and last name";
return (		
	<InputLayout group={this.props.group} id="fullName" labelCol={labelCol} inputCol={inputCol} label="Full Name" inputPattern="[^ ]+[ ][^ ].*" inputErr={inputErr} type="text" placeholder="Enter Full Name" value="" required="true" disabled="false" />	
)};

var FullNameComponent = React.createClass({
    render: page
});

module.exports = FullNameComponent;
