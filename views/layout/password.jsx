var React = require('react');
var InputLayout = require('../layout/form_input');

var page = function() { 
var inputCol = this.props.inputCol ? this.props.inputCol : "col-sm-10";
var labelCol = this.props.labelCol ? this.props.labelCol : "col-sm-2";
var id = this.props.id ? this.props.id : "password";
var label = this.props.label ? this.props.label : "Password";
var placeholder = this.props.placeholder ? this.props.placeholder : "Enter Password";
return (		
	<InputLayout group={this.props.group} id={id} labelCol={labelCol} inputCol={inputCol} label={label} inputPattern=".{6,}" inputErr="Please supply a password that is at least 6 characters" type="password" placeholder={placeholder} value="" required="true" disabled="false" />	
)};

var PasswordComponent = React.createClass({
    render: page
});

module.exports = PasswordComponent;
