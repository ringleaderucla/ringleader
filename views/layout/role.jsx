var React = require('react');
var SelectComponent = require('./form_select');

var page = function() {
var group = this.props.group ? this.props.group : "form-group"; 
var label = this.props.label ? this.props.label : "Role";
return (	
	<SelectComponent group={group} id="role" label={label} labelCol={this.props.labelCol} inputCol={this.props.inputCol} options={this.props.roles} />
)};

var RoleComponent = React.createClass({
    render: page
});

module.exports = RoleComponent;
