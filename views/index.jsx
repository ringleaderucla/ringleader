var React = require('react');
var DefaultLayout = require('./layout/master'); // import the master layout
var TimelineLayout = require('./layout/timeline');
var TaskTableLayout = require('./layout/task_table');

var page = function() {
var numberOfDaysLeft = "Wedding date has not been set yet!";
if (this.props.user.wedding.bigDay) {
	var diff = this.props.user.wedding.bigDay - new Date();
	if (diff < 0) { // wedding date has passed
		numberOfDaysLeft = "Congratulations!"
	}
	else {
		numberOfDaysLeft = Math.ceil(diff / (1000*60*60*24)) + " Days Left";
	}
}
var curDate = new Date();
curDate.setHours(0,0,0,0);
var thirtyDaysLaterDate = new Date();
thirtyDaysLaterDate.setDate(thirtyDaysLaterDate.getDate() + 30);
var tasks = [];
for (var i = 0; i < this.props.tasks.length; i++) {
	if (this.props.tasks[i].dueDate >= curDate && this.props.tasks[i].dueDate <= thirtyDaysLaterDate) {
		tasks.push(this.props.tasks[i]);
	}
}
return (
<DefaultLayout props={this.props}>
	<div className="container">
		<div className="row text-center">
			<h1><strong>{numberOfDaysLeft}</strong></h1>
			<br/>
		</div>
		<h2>Milestones</h2>
		<TimelineLayout user={this.props.user} milestones={this.props.milestones} />
		<h2>What's Coming Up?</h2>
		<br/>
		<TaskTableLayout user={this.props.user} tasks={tasks} home={true} />
		<h2>What's New?</h2>
		<br/>
		<TaskTableLayout user={this.props.user} updatedTasks={this.props.updatedTasks} />
	</div>
</DefaultLayout>
)};

var IndexComponent = React.createClass({
	render: page
});

module.exports = IndexComponent;
