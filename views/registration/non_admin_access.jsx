var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout

var page = function() { 
return (
	<DefaultLayout props={this.props} noNav='true' >
		<div className="container-small text-center">
			<h2 className="rl-red">Only the Administrator of the wedding can access the registration page! 
			If your Administrator has not completed the registration process you will not be able to access any other pages</h2>
		</div>
	</DefaultLayout>
)};

var NonAdminAccessComponent = React.createClass({
    render: page
});

module.exports = NonAdminAccessComponent;
