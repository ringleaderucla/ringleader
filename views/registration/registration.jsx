var React = require('react');
var ClientRenderLayout = require('../layout/client_render'); 
var EmailLayout = require('../layout/email');
var RoleLayout = require('../layout/role');
var FullNameLayout = require('../layout/fullname');
var helper = require('../../routes/helper/helper');

var page = function() {
	var btnStyle = {	
		minWidth: '120px',
		minHeight: '40px'
	};

	var txtStyle = {
		fontSize: "150%",
		textAlign: "right"
	};

	var txtStyleIta = {
		fontStyle: "italic",
		fontSize: "150%"
	};

	var milestoneBtnStyle = {
		textAlign: "left",
		display: "inline-block",
		fontSize: "150%"
	};

	var invite =
	<div className="row">
		{ this.props.invalidEmail ? <script src="/js/invalid_input.js" data-valid-fields='["fullName"]' data-invalid-field="email" data-err-msg="Email is already registered" /> : null }
		<div className="col-sm-12">
		 <form className="form-horizontal" role="form" method="post" action="/registration/add" encType="application/json">
			   <div className="form-group">
			   	    <FullNameLayout group=" " inputCol="col-sm-2" inputErr="Please supply first and last name" />
				<EmailLayout group=" " labelCol="col-sm-1" inputCol="col-sm-2"/>
				<RoleLayout group=" " labelCol="col-sm-1" inputCol="col-sm-2" roles={this.props.roles} />
				<div className="col-sm-2 text-center">
					 <button type="submit" id="submit" style={btnStyle} className="btn-rl btn-pad-small no-margin rl-bg-orange">Add</button>
				</div>
			   </div>
		 </form>
		</div>
	</div>;

	var editHeader = <th></th>;
	var deleteHeader = <th></th>;

	//non admin users cannot invite, edit, or delete
	if (!this.props.user.admin) {
		invite = null;
		editHeader = null;
		deleteHeader = null;
	}

	/*Default milestones*/
	var milestoneNames = this.props.milestoneNames;
	var milestoneBtnEntriesLeft = new Array();
	var milestoneBtnEntriesRight = new Array();
	var milestoneEntry = function(key) {
		return (
			<div key={key}>
				<img className='milestone-btn' height="25px" src="/images/add_btn.png"/>
				<img className='milestone-btn' height="25px" src="/images/added_icon.png"/>
				<img className='milestone-btn' height="25px" src="/images/close_btn.png"/>
				&nbsp;
				{milestoneNames[i]}
				<br/><br/>
			</div>
		)};
	for (var i = 0; i < milestoneNames.length; i++){
		if (i < milestoneNames.length / 2){
		   milestoneBtnEntriesLeft.push(milestoneEntry(i));
		}
		else {
		   milestoneBtnEntriesRight.push(milestoneEntry(i));  
		}
	}

	/*Current team members*/
	var team = JSON.stringify(this.props.team);

	return (
		<ClientRenderLayout props={this.props} noNav={this.props.user.wedding.registered ? false : true}>
			<link rel="stylesheet" href="/css/daypicker-style.css"/>
			<div className="container-small">
			  <br/>
			  <div id="myCarousel" className="carousel slide text-center" data-interval="false">
				{/* Wrapper for slides */}
				<div className="carousel-inner" role="listbox">

					{/*Step 1*/}
					<div className="item active">
						<div className="text-center"><h4>You're in! Now we're just going to need to collect a few details about your wedding. If you're not sure, you can always come back and make changes later.</h4></div>

						<h2><strong>When is the Big Day?</strong></h2>

						<div id="registration-calendar" data-weddingday={JSON.stringify(this.props.user.wedding.bigDay)}></div>
					</div>

					{/*Step 2*/}

					<div className="item">
						<div className="text-center"><h4>Let's pick out the people who are going to be helping you put together your big day. Add as many people as you can think of, and you can add more later.</h4></div>
						<h2><strong>Who's Going to Be Helping?</strong></h2>
						<div className="pack col-sm-8">
							<form id="user-form" className="form-horizontal" role="form">
								<FullNameLayout labelCol="col-sm-4" inputCol="col-sm-8" inputErr="Please supply first and last name" />
								<EmailLayout labelCol="col-sm-4" inputCol="col-sm-8"/>
								<RoleLayout labelCol="col-sm-4" inputCol="col-sm-8" roles={this.props.roles} />
								<div className="text-center">
									<button id="add-user-btn" type="submit" className="btn-rl rl-bg-orange">Add</button>
								</div>
							</form>
						</div>
						<div className="col-sm-4">
							<div className="rl-green">
								<h4><b> Your Wedding Team </b></h4>
							</div>
							<hr className="separator"></hr>
							<div style={txtStyleIta}>You</div>
							<hr className="separator"></hr>
							<div id="wedding-team" style={txtStyle}>
							</div>
						</div>
					</div>
				  
					{/*Step 3*/}
					<div className="item">
					   <div className="text-center"><h4>To help keep all the individual things you have to do in perspective, we're going to create some big milestones. Add the ones that are relevant, and we'll help you figure out when they need to get done.</h4></div>
						<h2><strong>What Are Your Milestones?</strong></h2>
						<br/>

						<div className="col-sm-6">

							<div style={milestoneBtnStyle}>
							{milestoneBtnEntriesLeft}
							</div>
					
						</div>
					
						<div className="col-sm-6">

							 <div style={milestoneBtnStyle}>
							 {milestoneBtnEntriesRight}
							 </div>
						</div>
					</div>
				</div>
			
				<br/>
				{/* Next Button */}
				<div id="next">
					<a id="next-btn" className="btn-rl rl-bg-red btn-pad-small" href="#myCarousel" role="button" data-slide="next">Next</a>
				</div>

				<div hidden="true" id="all-set">
					<button id="all-set-btn" className="btn-rl rl-bg-red btn-pad-small" role="button">All Set!</button>
				</div>

				<br/>
			
				{/* Indicators */}
				<div id="step-name"></div>
				<div>
					<button id="step-btn-0" value="Set the Date" className="btn btn-circle" data-target="#myCarousel" data-slide-to="0">1</button>
					<button id="step-btn-1" value="Pick your Team" className="btn btn-circle" data-target="#myCarousel" data-slide-to="1">2</button>
					<button id="step-btn-2" value="Set Milestones" className="btn btn-circle" data-target="#myCarousel" data-slide-to="2">3</button>
				</div>
				<script src="/js/libs/registration_libs.js"/> 
				<script src="/js/registration.js" data-team={team} data-step={this.props.step} />
				<script type="text/babel" src="/js/registration_calendar.js"/>
			  </div>
			</div>
		</ClientRenderLayout>
)};

var RegistrationComponent = React.createClass({
    render: page
});

module.exports = RegistrationComponent;
