var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var EmailPasswordLayout = require('./email_pw');
var config = require('../../config');

var page = function() { 
return (
	<DefaultLayout props={this.props}>
		<div className="container-small">
			<div className="text-center">
				<h3>Welcome back to {config.appName}. You remember your password, 
right?</h3>
				{this.props.verifyEmail ? <h4 style={{fontStyle: 
'italic', fontWeight: 'bold'}}>Check your email, you need to verify your 
account before you can log in!</h4> : null}
			</div>
			<br/>
			<div className="row">
				<div className="pack">
					{ this.props.invalid ? <script src="/js/invalid_input.js" data-valid-fields='["email"]' data-invalid-field="password" data-err-msg="Invalid password" /> : null }
					<EmailPasswordLayout action="/user/login" submitText="I'm Back!"/>
				</div>
				<br/>
				<div className="row text-center">
		        	<a href="/user/signup" className="btn btn-link rl-green">I need to create an account</a>
				</div>
			</div>
		</div>
    </DefaultLayout>
)};

var LoginComponent = React.createClass({
    render: page
});

module.exports = LoginComponent;

