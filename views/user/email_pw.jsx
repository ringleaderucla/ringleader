var React = require('react');
var EmailLayout = require('../layout/email');
var PasswordLayout = require('../layout/password');

var page = function() { 
return (
	<form id="user-form" className="form-horizontal" role="form" action={this.props.action} method="post" encType="application/json">
		{this.props.children}
		<EmailLayout/>
		<PasswordLayout/>
		<div className="text-right" style={{marginRight: "2%"}}>
			<div className="checkbox">
				<label><input type="checkbox" id="show-pwd" name="show-pwd"/>Show Password</label>
			</div>
		</div>
		<br/>
		<div className="row text-center">
			<button id="submit" type="submit" className="btn-rl rl-bg-red">{this.props.submitText}</button>
		</div>
		<script src="/js/login_signup.js"/>
	</form>
)};

var EmailPasswordComponent = React.createClass({
    render: page
});

module.exports = EmailPasswordComponent;

