var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var InputLayout = require('../layout/form_input');
var EmailLayout = require('../layout/email');

var page = function() { 
var user = this.props.userToEdit;
var roleView = <InputLayout id="role" type="text" labelCol="col-sm-5" inputCol="col-sm-4" label={<b>Role</b>} disabled="true" value={user.role} />;
var emailView = <EmailLayout labelCol="col-sm-5" inputCol="col-sm-4" label={<b>Email</b>} disabled="true" value={user.email} />;
if (this.props.user.admin && !user.admin) { //user is an admin and the profile user is editting is not an admin
	var RoleLayout = require('../layout/role');
	roleView = <div>
					<script src="/js/select_option.js" data-select-id="role" data-option={this.props.roles.indexOf(user.role)} />
					<RoleLayout label={<b>Role</b>} labelCol="col-sm-5" inputCol="col-sm-4" roles={this.props.roles} />
				</div>;
	emailView = <EmailLayout labelCol="col-sm-5" inputCol="col-sm-4" label={<b>Email</b>} value={user.email} />;
}
return (
    <DefaultLayout props={this.props}>
        <div className="container-small">
      		<div className="row text-center"><h1>Profile</h1></div>
			<br/>
			<form id="edit-profile-form" className="form-horizontal" role="form" action={"/user/profile/edit?id="+user._id} method="post" encType="application/json">
				<InputLayout id="firstName" label={<b>First Name</b>} labelCol="col-sm-5" inputCol="col-sm-4" type="text" value={user.firstName} placeholder="Enter First Name" required="true" inputErr="Please supply a first name" />
				<InputLayout id="lastName" label={<b>Last Name</b>} labelCol="col-sm-5" inputCol="col-sm-4" type="text" value={user.lastName} placeholder="Enter Last Name" required="true" inputErr="Please supply a last name" />
				{roleView}
				{ this.props.emailTaken ? <script src="/js/invalid_input.js" data-valid-fields='["firstName","lastName","role"]' data-invalid-field="email" data-err-msg="That email is already being used" /> : null }
				{emailView}
				{ this.props.emailTaken ? <script src="/js/restore_input.js" data-field="email" data-value={user.email} /> : null }
				{ this.props.user == this.props.userToEdit ? 
					<div className="text-center">
						<div className="checkbox">
							<label><input type="checkbox" id="dailyDigest" name="dailyDigest" defaultChecked={this.props.user.dailyDigest}/>Daily Digest</label>
						</div>
						<br/>
					</div>
					: null
				}
				<div className="row text-center">
					<button id="submit" type="submit" className="btn-rl btn-pad-small rl-bg-red">Update Profile</button>
				</div>
			</form>
       </div>
    </DefaultLayout>
)};

var EditProfileComponent = React.createClass({
    render: page
});

module.exports = EditProfileComponent;
