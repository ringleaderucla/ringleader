var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout

var page = function() {
var styleLabel = {
	textAlign: 'right'
};
var styleButton = {
	width: '290px'
};
return (
    <DefaultLayout props={this.props}>
        <div className="container-small">
      		<div className="row text-center"><h1>Profile</h1></div>
			<br/>
			<div className="row">
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="firstName"><b>First Name</b></label>
					<label className="control-label col-sm-6">{this.props.user.firstName}</label>
				</div>
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="lastName"><b>Last Name</b></label>
					<label className="control-label col-sm-6">{this.props.user.lastName}</label>
				</div>
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="role"><b>Role</b></label>
					<label className="control-label col-sm-6">{this.props.user.role}</label>
				</div>
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="email"><b>Email Address</b></label>
					<label className="control-label col-sm-6">{this.props.user.email}</label>
				</div>
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="mem"><b>Member Since</b></label>
					<label className="control-label col-sm-6">{this.props.user.created_at.toLocaleDateString()}</label>
				</div>
				<div className="col-sm-12  col-sm-offset-1">
					<label className="control-label col-sm-5" style={styleLabel} htmlFor="dd"><b>Daily Digest</b></label>
					<label className="control-label col-sm-6">{this.props.user.dailyDigest ? "On" : "Off"}</label>
				</div>
			</div>
			<br/>
			<div className="row text-center">
				<a href="/user/profile/edit" style={styleButton} id="edit_profile" className="btn-rl btn-pad-small rl-bg-red">Edit Profile</a>
			</div>
			<div className="row text-center">
				<a href="/user/profile/password" style={styleButton} id="edit_password_submit" className="btn-rl btn-pad-small rl-bg-red">Change Password</a>
			</div>
       </div>
    </DefaultLayout>
)};

var ProfileComponent = React.createClass({
    render: page
});

module.exports = ProfileComponent;
