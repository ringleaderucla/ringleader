var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var FullNameLayout = require('../layout/fullname'); 
var EmailPasswordLayout = require('./email_pw');
var config = require('../../config');

var page = function() { 
return (
	<DefaultLayout props={this.props}>
		<div className="container-small">
			<div className="text-center"><h3>Welcome to {config.appName}. To get started, we're going to need to create an account for you. We'll add other people in your wedding party later.</h3></div>
			<br/>
			<div className="row">
				<div className="pack">
					{ this.props.invalidEmail ? <script src="/js/invalid_input.js" data-valid-fields='["fullName"]' data-invalid-field="email" data-err-msg="Email is already registered" /> : null }
					<EmailPasswordLayout action="/user/signup" submitText="Let's Do It!">
						<FullNameLayout/>
					</EmailPasswordLayout>
				</div>
				<br/>
				<div className="row text-center">
		        	<a href="/user/login" className="btn btn-link rl-green">I already have an account</a>
				</div>
			</div>
		</div>
    </DefaultLayout>
)};

var SignupComponent = React.createClass({
    render: page
});

module.exports = SignupComponent;
