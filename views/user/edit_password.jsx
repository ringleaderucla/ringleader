var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var PasswordLayout = require('../layout/password');

var page = function() { 
	return (
    <DefaultLayout props={this.props}>
        <div className="container-small">
      		<div className="row text-center"><h1>Edit Password</h1></div>
				<br/>
				<form id="profile-form" className="form-horizontal" role="form" action="/user/profile/password" method="post" encType="application/json">
					{ this.props.invalidConfirm ? <script src="/js/invalid_input.js" data-valid-fields='["oldpassword","password"]' data-invalid-field="confirmpassword" data-err-msg="New password did not match confirmation" /> : (
						this.props.invalidPassword ? <script src="/js/invalid_input.js" data-valid-fields='[]' data-invalid-field="oldpassword" data-err-msg="Old password was incorrect" /> : null) }
					<PasswordLayout id="oldpassword" label="Old Password" labelCol="col-sm-5" inputCol="col-sm-4" placeholder="Enter Old Password" />
					<PasswordLayout id="password" label="New Password" labelCol="col-sm-5" inputCol="col-sm-4" placeholder="Enter New Password" />
					<PasswordLayout id="confirmpassword" label="Confirm New Password" labelCol="col-sm-5" inputCol="col-sm-4" placeholder="Confirm New Password" />

					<div className="row text-center">
						<button id="submit" type="submit" className="btn-rl btn-pad-small rl-bg-red">Update Password</button>
					</div>
				</form>
       </div>
    </DefaultLayout>
)};

var EditPasswordComponent = React.createClass({
    render: page
});

module.exports = EditPasswordComponent;
