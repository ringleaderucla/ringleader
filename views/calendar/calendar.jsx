var React = require('react');
var ClientRenderLayout = require('../layout/client_render');
var DefaultLayout = require('../layout/master'); // import the master layout

var page = function() { 
	
	var milestonesData = [];
	for(var i=0; i < this.props.milestones.length; i++){
		var dueDate = new Date(this.props.milestones[i].dueDate);
		milestonesData.push({
			name: this.props.milestones[i].name, 
         dueDate: dueDate
		});
	}

	var tasksData = [];
	for(var i=0; i < this.props.tasks.length; i++){
		var dueDate = new Date(this.props.tasks[i].dueDate);
		tasksData.push({
			name: this.props.tasks[i].name,
			dueDate: dueDate
		});
	}

	return (
	<ClientRenderLayout props={this.props}>
		<link rel="stylesheet" href="/css/daypicker-style.css"/>
		<div className="container-small">
			<div className="row text-center">
				<h1>Calendar</h1>
			</div>

			<div id="display-calendar" 
				data-milestones={JSON.stringify(milestonesData)}
				data-tasks={JSON.stringify(tasksData)}>
			</div>
			<script src="/js/libs/day_picker.js"/> 
			<script type="text/babel" src="/js/display_calendar.js"/>

			{/*TODO: sync with google calendar
			<div className="row text-center">
				<h4>Add your Milestones and Tasks to Your Calendar!</h4>
			</div>
			<div className="row">
				<div className="pack">
					<form className="form-horizontal" name="calendar-form" role="form" action="/calendar" method="get" encType="application/json">
						<div className="row text-center">
							<button type="submit" className="btn-rl rl-bg-red">Sync Now!</button>
						</div>
					</form>
				</div>
			</div>
			*/}
		</div>
    </ClientRenderLayout>
)};

var CalendarComponent = React.createClass({
    render: page
});

module.exports = CalendarComponent;
