var React = require('react');
var ClientRenderLayout = require('../layout/client_render'); 
var TaskFormLayout = require('./form'); 

var page = function() { 
return (
    <ClientRenderLayout props={this.props}>
		<div className="container">
			<h2>Edit Task</h2>
			<div className="text-right">
				<a href="/tasks" type="button" className="btn-rl btn-pad-xsmall rl-bg-orange" >Cancel</a>
			</div>
			<br/>
			<TaskFormLayout props={this.props} />
        </div>
    </ClientRenderLayout>
)};

var EditTaskComponent = React.createClass({
    render: page
});

module.exports = EditTaskComponent;
 
