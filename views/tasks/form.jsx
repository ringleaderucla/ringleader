var React = require('react');
var dateFormat = require('dateformat');
var InputLayout = require('../layout/form_input'); 
var SelectLayout = require('../layout/form_select'); 
var TextAreaLayout = require('../layout/textarea'); 

var TaskFormComponent = React.createClass({
    render: function() {
		var team = new Array();
		var leadIndex = -1;
		var milestoneIndex = -1;
		if (this.props.add) {
			leadIndex--;
			milestoneIndex--;
		}
		for (var i = 0; i < this.props.props.team.length; i++) {
			if (leadIndex == -1 && this.props.props.team[i]._id.toString() == this.props.props.taskToEdit.personInCharge.toString())
				leadIndex = i;
			team.push(this.props.props.team[i].firstName + " " + this.props.props.team[i].lastName + " (" + this.props.props.team[i].email + ")");
		}
		var milestones = new Array();
		for (var i = 0; i < this.props.props.milestones.length; i++) {
			if (milestoneIndex == -1 && this.props.props.milestones[i]._id.toString() == this.props.props.taskToEdit.parentMilestone.toString())
				milestoneIndex = i;
			milestones.push(this.props.props.milestones[i].name + " (" + dateFormat(this.props.props.milestones[i].dueDate, "mm/dd/yyyy") + ")");
		}
		return (
		<form id="task-form" className="form" role="form" action={"/tasks/" + (this.props.add ? "add" : "edit?id=" + this.props.props.taskToEdit._id)} method="post" encType="application/json">
			<div className="col-sm-6">
				<InputLayout value={this.props.add ? "" : this.props.props.taskToEdit.name} id="name" label="Name" inputErr="Please supply a name for the task" type="text" placeholder="Enter Task Name" />
				{leadIndex > 0 ? <script src="/js/select_option.js" data-select-id="lead" data-option={leadIndex} /> : null }
				<SelectLayout id="lead" label="Lead Contact" options={team} />
				<div id="input-calendar" data-date={this.props.add ? "" : this.props.props.taskToEdit.dueDate} data-label="Due Date" data-bad-date={this.props.props.badDate} data-max-date={dateFormat(this.props.props.user.wedding.bigDay, "mm/dd/yyyy")}></div>
				<link href='/css/react_input_calendar.css' rel='stylesheet' type='text/css' />
				<script src="/js/libs/input_calendar.js" />
				<script src="/js/input_calendar.js" type="text/babel" />
				{milestoneIndex > 0 ? <script src="/js/select_option.js" data-select-id="milestone" data-option={milestoneIndex} /> : null }
				<SelectLayout id="milestone" label="Milestone" options={milestones} />
			</div>
			<div className="col-sm-6">
				<TextAreaLayout value={this.props.add ? "" : this.props.props.taskToEdit.description} id="description" label="Description" rows="14" inputErr="Please supply a description for the task" placeholder="Enter Description" />
			</div>
			<div className="form-group text-left">
				<input type="submit" value="Done" className="btn-rl btn-pad-xsmall rl-bg-orange" />
			</div>
		</form> 
    )}
});

module.exports = TaskFormComponent;

