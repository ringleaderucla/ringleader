var React = require('react');
var DefaultLayout = require('../layout/master'); // import the master layout
var TaskTableLayout = require('../layout/task_table');

var page = function() { 

var curDate = new Date();
curDate.setHours(0,0,0,0);
var tasks = [];
for (var i = 0; i < this.props.tasks.length; i++) {
	if (this.props.tasks[i].dueDate >= curDate) {
		tasks.push(this.props.tasks[i]);
	}
}

return (
    <DefaultLayout props={this.props}>
        <div className="container">
            <h1>Tasks</h1>
			<TaskTableLayout user={this.props.user} tasks={tasks} />
			<br/>
			<a href="tasks/add" className="btn-rl btn-pad-xsmall rl-bg-orange">Add New Task</a>
        </div>
    </DefaultLayout>
)};

var TodosComponent = React.createClass({
    render: page
});

module.exports = TodosComponent;
