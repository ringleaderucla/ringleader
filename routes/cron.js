var schedule = require("node-schedule");
var express = require("express");
var router = express.Router();
var helper = require('./helper/helper');
var User = require('../models/user');
var Milestone = require('../models/milestone');
var Task = require('../models/task');
var config = require('../config');
var async = require('async');
var dateFormat = require('dateformat');

var sendDailyDigests = function() {
	console.log("Sending daily digests...");
	//get list of all users
	User.find({dailyDigest: true}, function(err,users){
		if (err){
			console.log(err);
			return;
		}

		//for each user, get all milestones and tasks the user is in charge of that were:
		//updated today, or is due soon 
		//then send email based on /views/email/daily_digest.jsx
		async.each(users, function(user, callback) {

			var updated = [];
			var dueSoon = [];
			var overdue = [];
			
			var addToLists = function(milestonesOrTasks) {
				var curDate = helper.getCurDate();
				var curDateTime = curDate.getTime();
				for(var i = 0; i < milestonesOrTasks.length; i++){
					if (milestonesOrTasks[i].updated_at >= curDate) {
						updated.push(milestonesOrTasks[i].name);
					}
					//passed due date and not complete
					if (milestonesOrTasks[i].dueDate < curDate && !milestonesOrTasks[i].status) { 
						overdue.push(milestonesOrTasks[i].name);
					}
					//due in 7 days and not complete
					//REMINDER: if you change the 7 days to something else, change the message in daily_digest.jsx
					else if (milestonesOrTasks[i].dueDate.getTime() - curDateTime < 604800000 && !milestonesOrTasks[i].status) { 
						dueSoon.push(milestonesOrTasks[i].name + " (" + dateFormat(milestonesOrTasks[i].dueDate, "mm/dd/yy") + ")");
					}
				}
			}

			Milestone.find({ 'personInCharge': user._id }, function(err, milestones) {
				if(err) {
					console.log(err);
				}
				if (milestones)
					addToLists(milestones);

				Task.find({ 'personInCharge': user._id }, function(err, tasks) {
					if(err) {
						console.log(err);
					}
					if (tasks)
						addToLists(tasks);
					
					//only send if there's anything to notify
					if (updated.length > 0 || dueSoon.length > 0 || overdue.length > 0) {
						helper.sendMail("email/daily_digest", user.email, 'This is a daily digest email from ' + config.appName, {
							name: user.firstName,
							updated: updated,
							dueSoon: dueSoon,
							overdue: overdue,
							url: config.domain
						});
					}
				});
			});
		}, function(err){
			if(err){
				console.log(err);
				return;
			}
		});
	});
}

//this function will be called at midnight every day
//see wikipedia page for CRON on the '* * * * *' syntax
schedule.scheduleJob('0 0 * * *', sendDailyDigests);

//TESTING DAILY DIGEST
//setTimeout(sendDailyDigests, 2000);

module.exports = router;
