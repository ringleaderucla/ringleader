var express = require('express');
var router = express.Router();
var helper = require('./helper/helper');

router.get('/', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res, { noUser: function() { res.redirect('/user/login'); }})) {
		helper.getMilestonesBasedOnPermissions(req.user, function(err, milestones) {
			if (err) throw err;
			helper.getTasksBasedOnPermissions(req.user, function(err, tasks) {
				if (err) throw err;
				helper.getTasksBasedOnPermissions(req.user, function(err, updatedTasks) {
					res.render('index', {
						name: "Home",
						user: req.user,
						milestones: milestones,
						tasks: tasks,
						updatedTasks: updatedTasks.slice(0, 5)
					});
				}, {updated_at: 'desc'});
			});
		});
	}
});

module.exports = router;
