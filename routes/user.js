var express = require('express');
var router = express.Router();
var passport = require('passport');
var helper = require('./helper/helper');
var User = require('../models/user');
var Wedding = require('../models/wedding');
var nev = require('../app').nev;
var config = require('../config');


//GET

router.get('/login', function(req, res) {
	if (req.user) //user is already logged in
		return res.redirect('../');
	//if invalid set to true a javascript will be called to highlight the password field and display invalid message
	invalid = false;
	if (req.query.invalid)
		invalid = true;
	res.render('user/login', {
		name: "Login",
		user: req.user,
		invalid: invalid,
		verifyEmail: req.query.verifyEmail
	});
});

router.get('/signup', function(req, res) {
	if (req.user)
		return res.redirect('../');

	//if invalidEmail set to true a javascript will be called to highlight the email field and display invalid message
	invalidEmail = false;
	if (req.query.invalidEmail)
		invalidEmail = true;

	res.render('user/signup', {
		name: "Sign Up",
		invalidEmail: invalidEmail
	});
});

router.get('/logout', function(req, res) {
	if (!req.user)
		return res.redirect('../');

    req.logout();
    res.redirect('login');
});

router.get('/profile', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res))
	{
		res.render('user/profile', {
			name: "Profile",
			user: req.user
		});
	}
});

router.get('/profile/edit', function(req, res) {
        // again, had to take this out to allow editing profiles during registration
	//if (helper.registeredWeddingRedirect(req, res)){
	
		// If email already taken
		emailTaken = false;
		if (req.query.emailTaken)
			emailTaken = true;

		//the user profile that the current user will be editting
		var userToEdit = req.user;
		var render = function() {
			res.render('user/edit_profile', {
				name: "Edit Profile",
				user: req.user,
				userToEdit: userToEdit,
				roles: helper.getNonAdminRoles(),
				emailTaken: emailTaken
			});
		}
		if (req.query.id && req.user.admin) { //editting another user only possible if admin
			User.findOne({_id: req.query.id}, function(err, user) {
				if (err) console.log(err);
				if (user) {
					userToEdit = user;
					render();
				}
				else //user does not exist then redirect to team page
					res.redirect('/team');
			});
		}
		else {
			render();
		}
	//}
});

router.get('/profile/password', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)){
		// check if new password matches confirm password
		invalidConfirm = false;
		if (req.query.invalidConfirm)
			invalidConfirm = true;

		// check if old password is correct
		invalidPassword = false;
		if (req.query.invalidPassword)
			invalidPassword = true;

		res.render('user/edit_password', {
			name: "Change Password",
			user: req.user,
			invalidConfirm: invalidConfirm,
			invalidPassword: invalidPassword
		});
	}
});

//POST

router.post('/login', function(req, res, next) {
	passport.authenticate('local', function(err, user, info) {
		if (err) { 
			console.log(err); 
			return next(err); 
		}
		if (!user) { 
			console.log(info.message); 
			return res.redirect('login/?invalid=true'); //sends parameter to set invalid to true
		}
		req.logIn(user, function(err) {
			if (err) { return next(err); }
			return res.redirect('../');
		});
	  })(req, res, next);
});

router.post('/signup', function(req, res) {
	var fullName = helper.getFirstLastName(req.body.fullName);

	//create new wedding
	var bigDay = new Date();
	bigDay.setFullYear(bigDay.getFullYear() + 1);
	var wedding = new Wedding({bigDay: bigDay});
	wedding.save();

	var email = req.body.email;
	var password = req.body.password;
	var newUser = User({
		email: email,
		password: password,
		wedding: wedding._id,
		firstName: fullName.firstName,
		lastName: fullName.lastName,
		role: helper.getAllRoles()[0],
		admin: true
	});

	var emailInUse = function() {
		var param = "?invalidEmail=true"; //sends parameter to notify user email is in use
    	return res.redirect('signup' + param);
	}

	//Must check to see that user email is not already in use
	User.findOne({email: email}, function(err, user) {
		if (err) console.log(err);
		if (user) //email in use
			return emailInUse();
		else {
			//create a temp user for the temp user model
			nev.createTempUser(newUser, function(err, newTempUser) {
				if(err){
					console.log(err);
				}
				if (newTempUser) {
					nev.registerTempUser(newTempUser, function(err) {
						if (err) console.log(err);
					});
					return res.redirect('login?verifyEmail=true');
				// user already exists in our temporary collection 
				} else {
					return emailInUse();
				}
			});
		}
	});
});

router.get('/email-verification/:URL', function(req, res) {
	var url = req.params.URL;
	var TempUser = nev.options.tempUserModel,
    query = {};
    query[nev.options.URLFieldName] = url;

    TempUser.findOne(query, function(err, tempUserData) {
      	if (err) {
        	console.log(err);
      	}

      	// temp user is found (i.e. user accessed URL before their data expired)
      	if (tempUserData) {
        	var userData = JSON.parse(JSON.stringify(tempUserData)); // copy data
          	var User = nev.options.persistentUserModel;
         	var user;

        	delete userData[nev.options.URLFieldName];
        	user = new User(userData);

        	// save the temporary user to the persistent user collection
        	User.register(new User({ 
				wedding: user.wedding,
				firstName: user.firstName,
				lastName: user.lastName,
				email: user.email,
				role: user.role,
				admin: user.admin }), 
				user.password, 
				function(err, user) {
				    if (err) {
			        	console.log(err);
				    }
				    if (user) 
					User.verifyEmail(user.authToken, function(err) { if (err) console.log(err); });

    			});

        		TempUser.remove(query, function(err) {
    				if (err){
    					console.log(err);
    				}
  				});
        // temp user is not found (i.e. user accessed URL after data expired, or something else...)
      	} else {
      	  	//error
      	}
    });


	res.redirect('/user/login');
});



router.post('/profile/edit', function(req, res) {
	//user editting own information
	var editSelf = !(req.user.admin && req.query.id != req.user._id);
	var save = function(user) {
		var redirect = function() {
			if (editSelf) 
				res.redirect('../profile');
			else //if admin is editting another user then redirect back to team page
				res.redirect('/team');
		};
		if (user) { //user exists then edit and save
			var saveChanges = function() { //saves changes
				user.firstName = req.body.firstName;
				user.lastName = req.body.lastName;
				if (editSelf) //admin can't mess with these attributes
					user.dailyDigest = req.body.dailyDigest ? true : false;

				if (req.body.role)
					user.role = req.body.role;
				user.save(function(err){ 
					if (err) console.log(err); 
					redirect();
				});
			};
			// check if email was changed
			if(req.body.email !== user.email && !editSelf){
				// check if email already exists in DB
				User.findByUsername(req.body.email, function(err, email_user){
					if (err) console.log(err);
					else if (email_user instanceof User)
						res.redirect('edit/?id='+req.query.id+'&emailTaken=true');
					else{
						user.email = req.body.email;
						saveChanges();
						helper.newPassword(user)
					}
				});
			} else {
				saveChanges();
			}
		}
		else
			redirect();
	};
	if (editSelf) {
		save(req.user);
	}
	else
	User.findOne({_id: req.query.id}, function(err, user) {
		if (err) console.log(err);
		save(user);
	});
});

router.post('/profile/password', function(req, res) {
	req.user.authenticate(req.body.oldpassword, function(err, result){
		if (err){
			console.log(err);
		// Validate old password
		} else if (!result) {
			console.log("Old password confirmation failed");
			res.redirect('password/?invalidPassword=true');
		}else if (req.body.password !== req.body.confirmpassword){
			console.log("New password confirmation error!");
			res.redirect('password/?invalidConfirm=true');
		} else {
			req.user.setPassword(req.body.password, function(err) {
				if (err)
					console.log(err);
				req.user.save(function(err){
					if(err)
						console.log(err);
					res.redirect('../profile');
				});
			});
		}
	});
});

module.exports = router;
