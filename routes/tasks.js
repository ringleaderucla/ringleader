var express = require('express');
var router = express.Router();
var helper = require('./helper/helper');
var Task = require('../models/task');
var User = require('../models/user');
var Milestone = require('../models/milestone');

router.get('/', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		helper.getTasksBasedOnPermissions(req.user, function(err, tasks) {
			if (err) console.log(err);
			res.render('tasks/tasks', {
				name: "To Dos",
				user: req.user,
				tasks: tasks
			});
		});
	}
});

router.get('/add', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		helper.getMilestonesBasedOnPermissions(req.user, function(err, milestones) {
			if (err) console.log(err);
			helper.getTeam(req.user, function(err, team) {
				if (err) console.log(err);
				res.render('tasks/add', {
					name: "Add Task",
					user: req.user,
					team: team,
					milestones: milestones,
					badDate: req.query.badDate
				});
			});
		});
	}
});

router.get('/edit', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)){
		if (req.query.id) {
			Task.findOne({_id: req.query.id}, function(err, task) {
				if (err) console.log(err);
				if (task && (req.user.admin || task.personInCharge.toString() == req.user._id.toString() || task.createdBy.toString() == req.user._id.toString())) {
					helper.getMilestonesBasedOnPermissions(req.user, function(err, milestones) {
						if (err) console.log(err);
						helper.getTeam(req.user, function(err, team) {
							if (err) console.log(err);
							res.render('tasks/edit', {
								name: "Edit Task",
								user: req.user,
								team: team,
								milestones: milestones,
								taskToEdit: task,
								badDate: req.query.badDate
							});
						});
					});
				}
				else //task does not exist then redirect to tasks page
					res.redirect('/tasks');
			});
		}
		else {
			res.redirect('/tasks');
		}
	}
});

router.get('/delete', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		//there must be an id provided to delete a task
		//the user must have privileges to delete the task
		if (req.query.id) {
			Task.findOne({_id: req.query.id}, function(err, task) {
				if (err) console.log(err);
				if (task && (req.user.admin || task.personInCharge.toString() == req.user._id.toString() || task.createdBy.toString() == req.user._id.toString())) {
					task.remove(function(err) {
						if (err) console.log(err);
						res.redirect('/tasks');
					});
				}
				else
					res.redirect('/tasks');
			});
		}
		else
			res.redirect('/tasks');
	}
});

router.get('/status', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		var redirect = function() {
			if (req.query.home) {
				res.redirect('../');
			}
			else {
				res.redirect('/tasks');
			}
		}
		//there must be an id provided to change the status of the task
		//the user must have privileges to change the status of the task
		if (req.query.id) {
			Task.findOne({_id: req.query.id}, function(err, task) {
				if (err) console.log(err);
				if (task && (req.user.admin || task.personInCharge.toString() === req.user._id.toString() || task.createdBy.toString() === req.user._id.toString())) {
					task.status = !task.status;
					task.statusChangeDate = new Date();
					task.updateDescription = "Status changed"
					task.save(function(err) { 
						if (err) console.log(err);
						redirect();
					});
				}
				else {
					if (!task)
						console.log("Task does not exist");
					else
						console.log("No permission to change status");
					redirect();
				}
			});
		}
		else
			redirect();
	}
});

router.post('/add', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		var email = helper.getEmailFrom(req.body.lead);
		var name = req.body.name.trim();
		if (email.error) {
			console.log(email.error);
			res.redirect('/tasks/add'); 
		}
		else {
			var dates = req.body.milestone.match(/[\d]+\/[\d]+\/[\d]+/g);
			var lastIndex = req.body.milestone.lastIndexOf(dates[dates.length-1]);
			var milestoneName = req.body.milestone.substring(0, lastIndex-1).trim();
			var milestoneDueDate = new Date(dates[dates.length - 1]);
			var milestoneQuery = {name: milestoneName, dueDate: helper.getQueryDateRange(milestoneDueDate)};
			var dueDate = new Date(req.body.date);
			dueDate.setHours(0,0,0,0);
			if (!dueDate || dueDate < helper.getCurDate() || dueDate > milestoneDueDate) { //dueDate must be within current milestone time frame (should be checked in input_calendar.js)
				res.redirect('/tasks/add?badDate=true');
			}
			else {
				User.findOne({email: email.email}, function(err, user) {
					if (err) console.log(err);
					if (user) {
						Milestone.findOne(milestoneQuery, function(err, milestone) {
							if (err) console.log(err);
							if (milestone) {
								var task = new Task({
									wedding: req.user.wedding._id, 
									name: name, 
									description: req.body.description,
									createdBy: req.user, 
									personInCharge: user,
									parentMilestone: milestone,
									dueDate: dueDate,
									updateDescription: "New task added"
								});
								task.save(function(err) {
									if (err) console.log(err);
									res.redirect('/tasks');
								});
							}
							else {
								console.log("Task Add: Could not query: " + milestoneQuery);
								res.redirect('/tasks/add');
							}
						});
					}
					else {
						console.log("Task Add: Could not query: " + email.email);
						res.redirect('/tasks/add');
					}
				});
			}
		}
	}
});

router.post('/edit', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		if (!req.query.id)
			return res.redirect('/tasks');
		var email = helper.getEmailFrom(req.body.lead);
		var name = req.body.name.trim();
		if (email.error) {
			console.log(email.error);
			res.redirect('/tasks/edit?id=' + req.query.id); 
		}
		else {
			var dates = req.body.milestone.match(/[\d]+\/[\d]+\/[\d]+/g);
			var lastIndex = req.body.milestone.lastIndexOf(dates[dates.length-1]);
			var milestoneName = req.body.milestone.substring(0, lastIndex-1).trim();
			var milestoneDueDate = new Date(dates[dates.length - 1]);
			var milestoneQuery = {name: milestoneName, dueDate: helper.getQueryDateRange(milestoneDueDate)};
			var dueDate = new Date(req.body.date);
			dueDate.setHours(0,0,0,0);
			if (!dueDate || dueDate < helper.getCurDate() || dueDate > milestoneDueDate) { //dueDate must be within current milestone time frame (should be checked in input_calendar.js)
				res.redirect('/tasks/edit?badDate=true&id=' + req.query.id);
			}
			else {
				User.findOne({email: email.email}, function(err, user) {
					if (err) console.log(err);
					if (user) {
						Milestone.findOne(milestoneQuery, function(err, milestone) {
							if (err) console.log(err);
							if (milestone) {
								Task.findOne({_id: req.query.id}, function(err, task) {
									var updateDescription = "";
									if (task.name !== name)
										updateDescription += "Name";
									if (task.description !== req.body.description) {
										if (updateDescription.length > 0)
											updateDescription += ", description";
										else
											updateDescription += "Description";
									}
									if (task.personInCharge.toString() !== user._id.toString()) {
										if (updateDescription.length > 0)
											updateDescription += ", lead contact";
										else
											updateDescription += "Lead contact";
									}
									if (task.parentMilestone.toString() !== milestone._id.toString()) {
										if (updateDescription.length > 0)
											updateDescription += ", associated milestone";
										else
											updateDescription += "Associated milestone";
									}
									if (task.dueDate.getDate() != dueDate.getDate() || task.dueDate.getFullYear() != dueDate.getFullYear() || task.dueDate.getMonth() != dueDate.getMonth()) {
										if (updateDescription.length > 0)
											updateDescription += ", due date";
										else
											updateDescription += "Due date";
									}
									if (updateDescription.length > 0)
										updateDescription += " changed";
									if (updateDescription.length > 0) {
										task.name = name;
										task.description = req.body.description;
										task.personInCharge = user;
										task.parentMilestone = milestone;
										task.dueDate = dueDate;
										task.updateDescription = updateDescription;
										task.save(function(err) {
											if (err) console.log(err);
											res.redirect('/tasks');
										});
									}
									else
										res.redirect('/tasks');
								});
							}
							else {
								console.log("Task Edit: Could not query: " + JSON.stringify(milestoneQuery));
								res.redirect('/tasks/edit?id=' + req.query.id);
							}
						});
					}
					else {
						console.log("Task Edit: Could not query email: " + email.email);
						res.redirect('/tasks/edit?id=' + req.query.id);
					}
				});
			}
		}
	}
});

module.exports = router;
