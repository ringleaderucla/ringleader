var express = require('express')
var config = require('../config');
var app = require('../app').app;
var router = express.Router();
var helper = require('./helper/helper');
var User = require('../models/user');
var Milestone = require('../models/milestone');

//only admins can access registration pages

var milestoneNames = new Array();

milestoneNames.push("Guest List Finalized");
milestoneNames.push("Bachelor Party Finalized");
milestoneNames.push("Bachelorette Party Finalized");
milestoneNames.push("Venue Booked");
milestoneNames.push("Rehearsal Dinner Finalized");
milestoneNames.push("Bride Attire Purchased");

router.get('/', function(req, res) {
    if (!req.user || !req.user.admin)
		return res.redirect('../');
	helper.getTeam(req.user, function(err, team) {
		if (err) console.log(err);
		res.render('registration/registration', {
			name: "Registration",
			user: req.user,
			roles: helper.getNonAdminRoles(),
			step: req.query.step,
			team: team,
			milestoneNames: milestoneNames
		})
	});
});
    
router.post('/complete', function(req, res) {
    if (!req.user || !req.user.admin)
		return res.redirect('../');

    var bigDay = req.body.bigDay;
    if(bigDay) {
	    req.user.wedding.bigDay = new Date(bigDay);
	}
    req.user.wedding.registered = true;
    req.user.wedding.save(function(err) {
		if (err) console.log(err);

		var stateArray = req.body.milestones;
		Milestone.find({_id: { $in: req.user.wedding.milestones }, name: { $in: milestoneNames }}, function(err, milestones) {
			if (err) console.log(err);
			/*Create default due dates for milestones*/
			var defaultDescription = "Add a description!";

			var curDate = helper.getCurDate();
			var today = curDate.getDate();
			var thisMonth = curDate.getMonth();
			var thisYear = curDate.getFullYear();
		
			var curBigDay = req.user.wedding.bigDay;
			var weddingDay = curBigDay.getDate();
			var weddingMonth = curBigDay.getMonth();
			var weddingYear = curBigDay.getFullYear();
		
			var daysInBtw = (weddingYear - thisYear) * 365 + (weddingMonth - (thisMonth + 1)) * 30 + (weddingDay - today);
			var nextDueDateIncrement = Math.max(Math.floor(daysInBtw / 10) - 5, 1);
			var defaultDates = new Array();
			for (var i = 0; i < milestoneNames.length; i++){
				today += nextDueDateIncrement;
				while (today >= 29){
					today -= 28;
					thisMonth++;
					if (thisMonth >= 11){
				   		thisMonth = 0;
						thisYear++;
					}
				}
	
				var tempDate = new Date();
				tempDate.setHours(0,0,0,0);
				tempDate.setDate(today);
				tempDate.setMonth(thisMonth);
				tempDate.setFullYear(thisYear);
				defaultDates.push(tempDate);
			}
			for (var i = 0; i < stateArray.length; i++) {
				if (stateArray[i] == 1) {
					var milestoneName = milestoneNames[i];
					var dueDate = defaultDates[i];
					var skip = false;
					if (milestones) {
						//if another milestone is found to have same name and due date do not add this one
						for (var j = 0; j < milestones.length; j++) {
							if (milestones[j].name === milestoneName && milestones[j].dueDate.getDate() == dueDate.getDate() && milestones[j].dueDate.getMonth() == dueDate.getMonth() && milestones[j].dueDate.getFullYear() == dueDate.getFullYear()) {
								skip = true;
								break;
							}
						}
					}
					if (!skip) {
						var milestone = new Milestone({		    
							wedding: req.user.wedding._id,
							name: milestoneName,
							createdBy: req.user,
							personInCharge: req.user,
							description: defaultDescription,
							dueDate: dueDate
						});
						milestone.save(function(err) { 
							if(err) console.log(err);
						});
					}
				}
			}
		});
		/*Add or update users*/
		var users = req.body.weddingTeam;
		for (var i = 0; i < users.length; i++) {
			if (users[i].fullName) {
				var fullName = helper.getFirstLastName(users[i].fullName);
				if (fullName.error) {
					console.log(fullName.error);
					//TODO: let user know the error somehow
				}
				else {
					helper.registerTeamMember(req.user.wedding._id, fullName.firstName, fullName.lastName, users[i].email, users[i].role, 
						function(err) { 
							console.log(err)
							//TODO: let user know the error somehow
						},
						function(user) {
							
						});
				}
			}
			else {
				//remove these users
				if (users[i].willDelete) {
					User.findOne({_id: users[i]._id}, function(err, user) {
						if (err) console.log(err);
						if (user) {
							user.remove(function(err) {
								if (err) console.log(err);
							});
						}
					});
				}
				else {
					if (users[i].updated) {
						var emailChanged = users[i].emailChanged
						//update these users
						User.findOneAndUpdate({_id: users[i]._id}, 
							{firstName: users[i].firstName, lastName: users[i].lastName, email: users[i].email, role: users[i].role}, 
							{new: true}, function(err, user) {
							if (err) console.log(err);
							if (emailChanged) {
								helper.newPassword(user);
							}
						});
					}
				}
			}
		}
		res.redirect('../../');   
	});

});

module.exports = router;
