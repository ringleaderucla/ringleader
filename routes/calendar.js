var express = require('express');
var router = express.Router();
var helper = require('./helper/helper');

function SyncCalendar(){
	// TODO: Google Calendar API here
}

router.get('/', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {

		var render = function(milestones, tasks) {
			res.render('calendar/calendar', {
				name: "Calendar",
				user: req.user,
				milestones: milestones,
				tasks: tasks
			});
		};

		helper.getMilestonesBasedOnPermissions(req.user, function(err, milestones) {
			if (err) throw err;
			helper.getTasksBasedOnPermissions(req.user, function(err, tasks){
				if (err) throw err;
				render(milestones, tasks);
			});
		});
	}
});



module.exports = router;
