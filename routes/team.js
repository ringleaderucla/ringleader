var express = require('express');
var config = require('../config');
var app = require('../app').app;
var router = express.Router();
var helper = require('./helper/helper');
var User = require('../models/user');

router.get('/', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {

		//if invalidEmail set to true a javascript will be called to highlight the email field and display invalid message
		invalidEmail = false;
		if (req.query.invalidEmail)
			invalidEmail = true;

		var render = function(team) {
			res.render('team/team', {
				name: "Team",
				user: req.user,
				team: team,
				roles: helper.getNonAdminRoles(),
				invalidEmail: invalidEmail
			})
		};
		helper.getTeam(req.user, function(err, team) {
			if (err) throw err;
			render(team);
		});
	}
});

router.get('/edit', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		if (req.user.admin) { //only admins can edit other team members' information
			res.redirect('/user/profile/edit?id=' + req.query.id);
		}
		else {
			res.redirect('/team')
		}
	}
});

router.get('/delete', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		//there must be an id provided to delete a user
		//the id must not be the current user's id
		//the id must exist in the current wedding team
		if (req.query.id && req.query.id != req.user._id && req.user.wedding.users.indexOf(req.query.id) >= 0) {
			var User = require('../models/user');
			User.findOne({_id: req.query.id}, function(err, user) {
				if (err) console.log(err);
				if (user) {
					user.remove(function(err) {
						if (err) console.log(err);
						res.redirect('/team');
					});
				}
				else {
					res.redirect('/team');
				}
			});
		}
		else
			res.redirect('/team');
	}
});

router.post('/add', function(req, res) {
	if (req.user) {
		var fullName = helper.getFirstLastName(req.body.fullName);
		if (fullName.error) {
			console.log(fullName.error);
			res.redirect('/team');
		}
		else {
			helper.registerTeamMember(req.user.wedding._id, fullName.firstName, fullName.lastName, req.body.email, req.body.role, 
				function(err) { 
					param = "/?invalidEmail=true"; //sends parameter to notify user email is in use
					return res.redirect('/team' + param);
				},
				function(user) {
					//if the add came from registration page go back to it
					if (req.query.fromRegistration)
						res.redirect('/registration?step=' + req.query.fromRegistration.toString());
					else
						res.redirect('/team');
				});
		}
	}
});

module.exports = router;
