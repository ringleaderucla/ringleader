var app = require("../../app").app;
var config = require("../../config");
var User = require("../../models/user");
var Milestone = require("../../models/milestone");
var Task = require("../../models/task");
var Wedding = require("../../models/wedding");

var helper = {
	/* redirects
	{
		registered: function()
		noUser:	function()
	}
	*/
	registeredWeddingRedirect: function(req, res, redirects) {
		if (!redirects)
			redirects = {};
		if (req.user) { //user is logged in
			if (req.user.wedding.registered) { //user's wedding is registered
				if (redirects.registered)
					redirects.registered();
				return true;
			}
			else { //if user's wedding is not registered, go to registration page
				res.redirect('/registration');
			}
		}
		else {
			if (redirects.noUser)
				redirects.noUser();
			else
				res.redirect('/');
		}
		return false;
	},
	getFirstLastName: function(fullName) {
		if (!fullName)
			return { error: "Undefined fullName" };
		
		var separatedName = fullName.split(" "); //first name and whatever after first space will be last name
		
		if (separatedName.length < 2) //only first name was given
			return { error: "Requires first and last name" };

		//store first and last name
		var firstName = separatedName[0];
		var lastName = separatedName.slice(1).join(" ");

		return { firstName: firstName, lastName: lastName };
	},
	getNonAdminRoles: function() {
		return User.schema.path('role').enumValues.slice(1);
	},
	getAllRoles: function() {
		return User.schema.path('role').enumValues;
	},
	getRandomPassword: function() {
		var password = "";
		var possible = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789";
		//generate random password
		for (var i = 0; i < 8; i++)
			password += possible.charAt(Math.floor(Math.random() * possible.length));
		return password;
	},
	sendMail: function(view, to, subject, messageObject) {
		var message = {
			to: to, 
			subject: subject
		};
		for (var m in messageObject) {
			message[m] = messageObject[m];
		}
		app.mailer.send(view, message, function (err) {
			if (err) {
				// handle error 
				console.log(err);
				return;
			}
		});
	},
	getMilestonesBasedOnPermissions: function(user, cb) {
		//query all
		var query = {'_id': { $in: user.wedding.milestones}};
		if (!user.admin) { //if user is not an admin look at permissions
			if (!user.permissions.read.milestone) { //user cannot see others' milestones
				//can see assigned milestone or milestone they created or milestones they have tasks assigned to
				query = { $or: [{'_id': { $in: user.milestones }}, 
								{'createdBy': user._id},
							    {'tasks': { $in: user.tasks }}] };
			}
		}
		Milestone.find(query)
			.sort({dueDate: 'asc'})
			.populate([{path: 'personInCharge', select: 'role firstName lastName'}])
			.exec(function(err, milestones) {
				cb(err, milestones);
			});
	},
	getTasksBasedOnPermissions: function(user, cb, sort) {
		if (!sort)
			sort = {dueDate: 'asc'};
		//query all
		var query = {'_id': { $in: user.wedding.tasks}};
		if (!user.admin) { //if user is not an admin look at permissions
			if (!user.permissions.read.task) { //user cannot see others' tasks
				//can see assigned task or task they created or task that belongs to the milestone they're in charge of
				query = { $or: [{'_id': { $in: user.tasks }},
								{'parentMilestone': { $in: user.milestones }},
								{'createdBy': user._id}] };
			}
		}
		Task.find(query)
			.sort(sort)
			.populate([{path: 'personInCharge', select: 'role firstName lastName'},
						{path: 'parentMilestone', select: 'name'}])
			.exec(function(err, tasks) {
				cb(err, tasks);
			});
	},
	getTeam: function(user, cb) {
		Wedding.findOne(user.wedding).populate('users').exec(function(err, wedding) {
			cb(err, (wedding ? wedding.users : null));
		});
	},
	getEmailFrom: function(text, delimiter) {
		var emails = text.match(/([a-zA-Z0-9._-]+@[a-zA-Z0-9._-]+\.[a-zA-Z0-9._-]+)/gi);
		if (emails.length == 0)
			return {error: "No email found!"};
		else
			return {email: emails[0]};
	},
	getCurDate: function() {
		var curDate = new Date()
		curDate.setHours(0,0,0,0);
		return curDate;
	},
	getQueryDateRange: function(date) {
		var lowerDate = new Date(date);
		lowerDate.setHours(0,0,0,0);
		var upperDate = new Date(date);
		upperDate.setHours(24,0,0,0);
		return {$gte: lowerDate, $lt: upperDate}
	}
}

helper.registerTeamMember = function(wedding, firstName, lastName, email, role, errCb, cb) {
	var password = helper.getRandomPassword();

	User.register(new User({ 
		wedding: wedding,
		firstName: firstName,
		lastName: lastName,
		email: email,
		role: role,
		admin: false }), 
		password, 
		function(err, user) {
			if (err) {
				errCb(err);
			}
		
			if (user) {
				//Mail the user their password
				helper.sendMail("email/email_invite", email, "You've been invited to " + config.appName + "!", {
					password: password,
					url: config.domain
				});
		
				//Verify user so that they can log in with the password and so that admin can see the person they invited
				User.verifyEmail(user.authToken, function(err) { if (err) console.log(err); });
			}
			cb(user);
		});
}
helper.newPassword = function(user) {
	var password = helper.getRandomPassword();
	//reset user's password and send email with new password
	user.setPassword(password, function(err) {
		if (err) console.log(err);
		user.save(function(err) {
			if (err) console.log(err);
			console.log("Mailing password to " + user.email);
			//Mail the user their password
			helper.sendMail("email/email_invite", user.email, "You've been invited to " + config.appName + "!", {
				password: password,
				url: config.domain
			});
		});
	});
}

module.exports = helper
