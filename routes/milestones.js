var express = require('express');
var router = express.Router();
var helper = require('./helper/helper');

var User = require('../models/user');
var Milestone = require('../models/milestone');
var Task = require('../models/task');

router.get('/', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		// Render the page with two props (user and milestones)
		var render = function(milestones) {res.render('milestones/milestones', {
			name: "Milestones",
			user: req.user,
			milestones: milestones
		})};
		// Find all the milestones belonging to current wedding
		// and those that are viewable by the user
		helper.getMilestonesBasedOnPermissions(req.user, function(err, milestones) {
			if (err) console.log(err);
			render(milestones);
		});
	}
});

router.get('/add', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		helper.getTeam(req.user, function(err, team) {
			if (err) console.log(err);
			res.render('milestones/add', {
				name: "Add Milestone",
				user: req.user,
				team: team,
				duplicate: req.query.duplicate,
				badDate: req.query.badDate
			});
		});
	}
});

router.get('/edit', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)){
		if (req.query.id) {
			Milestone.findOne({_id: req.query.id}, function(err, milestone) {
				if (err) console.log(err);
				if (milestone && (req.user.admin || milestone.personInCharge.toString() == req.user._id.toString() || milestone.createdBy.toString() == req.user._id.toString())) {
					helper.getTeam(req.user, function(err, team) {
						if (err) console.log(err);
						res.render('milestones/edit', {
							name: "Edit Milestone",
							user: req.user,
							team: team,
							milestoneToEdit: milestone,
							duplicate: req.query.duplicate,
							badDate: req.query.badDate
						});
					});
				}
				else //milestone does not exist or user does not have access to them then redirect to milestones page
					res.redirect('/milestones');
			});
		}
		else {
			res.redirect('/milestones');
		}
	}
});

router.get('/delete', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		//there must be an id provided to delete a milestone
		//the user must have privileges to delete the milestone
		if (req.query.id) {
			Milestone.findOne({_id: req.query.id}, function(err, milestone) {
				if (err) console.log(err);
				if (milestone && (req.user.admin || milestone.personInCharge.toString() == req.user._id.toString() || milestone.createdBy.toString() == req.user._id.toString())) {
					milestone.remove(function(err) {
						if (err) console.log(err);
						res.redirect('/milestones');
					});
				}
				else
					res.redirect('/milestones');
			});
		}
		else
			res.redirect('/milestones');
	}
});

router.get('/status', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		var redirect = function() {
			res.redirect('/milestones');
		}
		//there must be an id provided to change the status of the milestone
		//the user must have privileges to change the status of the milestone
		if (req.query.id) {
			Milestone.findOne({_id: req.query.id}, function(err, milestone) {
				if (err) console.log(err);
				if (milestone && (req.user.admin || milestone.personInCharge.toString() === req.user._id.toString() || milestone.createdBy.toString() === req.user._id.toString())) {
					milestone.status = !milestone.status;
					milestone.statusChangeDate = new Date();
					milestone.save(function(err) { 
						if (err) console.log(err);
						redirect();
					});
				}
				else {
					if (!milestone)
						console.log("Milestone does not exist");
					else
						console.log("No permission to change status");
					redirect();
				}
			});
		}
		else
			redirect();
	}
});

router.post('/add', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)) {
		var email = helper.getEmailFrom(req.body.lead);
		var name = req.body.name.trim();
		if (email.error) {
			console.log(email.error);
			res.redirect('/milestones/add'); 
		}
		else {
			var dueDate = new Date(req.body.date);
			dueDate.setHours(0,0,0,0);
			//dueDate must be within current wedding time frame (should be checked in input_calendar.js)
			if (!dueDate || dueDate < helper.getCurDate() || dueDate > req.user.wedding.bigDay) { 
				res.redirect('/milestones/add?badDate=true'); 
			}
			else {
				User.findOne({email: email.email}, function(err, user) {
					if (err) console.log(err);
					if (user) {
						Milestone.findOne({wedding: req.user.wedding._id, name: name, dueDate: helper.getQueryDateRange(dueDate)}, function(err, milestone) {
							if (err) console.log(err);
							//if another milestone is found to have same name and due date do not add this one
							if (milestone) {
								res.redirect('/milestones/add?duplicate=true');
							}
							else {
								var milestone = new Milestone({
								    wedding: req.user.wedding._id, 
								    name: name, 
								    createdBy: req.user, 
								    personInCharge: user,
								    description: req.body.description,
								    dueDate: dueDate
								});
								milestone.save(function(err) {
									if (err) console.log(err);
									res.redirect('/milestones');
								});
							}
						});
					}
					else {
						console.log("Milestone Add: Could not query: " + email.email);
						res.redirect('/milestones/add');
					}
				});
			}
		}
	}
});

router.post('/edit', function(req, res) {
	if (helper.registeredWeddingRedirect(req, res)){
		if (!req.query.id)
			return res.redirect('/milestones');
		var email = helper.getEmailFrom(req.body.lead);
		var name = req.body.name.trim();
		if (email.error) {
			console.log(email.error);
			res.redirect('/milestones/edit?id=' + req.query.id); 
		}
		else {
			var dueDate = new Date(req.body.date);
			dueDate.setHours(0,0,0,0);
			//dueDate must be within current wedding time frame (should be checked in input_calendar.js)
			if (!dueDate || dueDate < helper.getCurDate() || dueDate > req.user.wedding.bigDay) { 
				res.redirect('/milestones/edit?badDate=true&id=' + req.query.id); 
			}
			else {
				User.findOne({email: email.email}, function(err, user) {
					if (err) console.log(err);
					if (user) {
						Milestone.findOne({wedding: req.user.wedding._id, name: name, dueDate: helper.getQueryDateRange(dueDate)}, function(err, milestone) {
							if (err) console.log(err);
							//if another milestone is found to have same name and due date do not edit this one
							if (milestone && milestone._id.toString() != req.query.id.toString()) {
								res.redirect('/milestones/edit?duplicate=true&id=' + req.query.id);
							}
							else {
								Milestone.findOne({_id: req.query.id}, function(err, milestone) {
									milestone.name = name;
									milestone.personInCharge = user;
									milestone.dueDate = dueDate;
									milestone.description = req.body.description;
									milestone.save(function(err) {
										if (err) console.log(err);
										res.redirect('/milestones');
									});
								});
							}
						});
					}
					else {
						console.log("Milestone Edit: Could not query email: " + email.email);
						res.redirect('/milestones/edit?id=' + req.query.id);
					}
				});
			}
		}
	}
});

module.exports = router;
